package com.workshops.managing.system.utils;

import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RegistrationListener implements
    ApplicationListener<OnRegistrationCompleteEvent> {

  @Autowired
  private AccountService accountService;

  @Autowired
  private JavaMailSender mailSender;

  @Override
  public void onApplicationEvent(OnRegistrationCompleteEvent event) {
    this.confirmRegistration(event);
  }

  private void confirmRegistration(OnRegistrationCompleteEvent event) {
    Account account = accountService.getAccountByLogin(event.getAccountLogin());
    String token = UUID.randomUUID().toString();

    accountService.createVerificationToken(account, token);

    String recipientAddress = account.getEmail();
    String subject = "Registration Confirmation";
    String confirmationUrl = "http://localhost:8080/registrationConfirm.html?token=" + token;

    SimpleMailMessage email = new SimpleMailMessage();
    email.setTo(recipientAddress);
    email.setSubject(subject);
    email.setText("Welcome to workshops and trainings managing system.\n" +
        "Before you login to our site, please verify your email address.\n" +
        "Click this link to complete verification process." + confirmationUrl);
    mailSender.send(email);
  }
}