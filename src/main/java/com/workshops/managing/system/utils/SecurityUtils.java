package com.workshops.managing.system.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.workshops.managing.system.model.Response;
import com.workshops.managing.system.model.exception.AppException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public final class SecurityUtils {

  private static final ObjectMapper mapper = new ObjectMapper();


  private SecurityUtils() {
  }

  public static void sendResponse(HttpServletResponse response, int status, Object object) throws IOException {
    response.setContentType(Consts.CONTENT_TYPE_JSON);
    PrintWriter writer = response.getWriter();
    writer.write(mapper.writeValueAsString(object));
    response.setStatus(status);
    writer.flush();
    writer.close();
  }

  public static void sendError(HttpServletResponse response, Exception exception, int status, String message) throws IOException {
    response.setContentType(Consts.CONTENT_TYPE_JSON);
    response.setStatus(status);
    PrintWriter writer = response.getWriter();
    AppException appException = AppException.of(exception);
    writer.write(mapper.writeValueAsString(new Response(String.valueOf(status), message, appException)));
    writer.flush();
    writer.close();
  }

}