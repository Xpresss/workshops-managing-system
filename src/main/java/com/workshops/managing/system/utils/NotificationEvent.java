package com.workshops.managing.system.utils;

import com.workshops.managing.system.model.entity.Notification;
import org.springframework.context.ApplicationEvent;

public class NotificationEvent extends ApplicationEvent {

  private String login;
  private Long eventId;
  private Notification.NotificationType notificationType;

  public NotificationEvent(String login, Long eventId, Notification.NotificationType notificationType) {
    super(notificationType);
    this.login = login;
    this.eventId = eventId;
    this.notificationType = notificationType;
  }

  public String getLogin() {
    return login;
  }
  public void setLogin(String login) {
    this.login = login;
  }

  public Long getEventId() {
    return eventId;
  }
  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public Notification.NotificationType getNotificationType() {
    return notificationType;
  }
  public void setNotificationType(Notification.NotificationType notificationType) {
    this.notificationType = notificationType;
  }
}
