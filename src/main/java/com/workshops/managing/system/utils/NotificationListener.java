package com.workshops.managing.system.utils;

import com.workshops.managing.system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class NotificationListener implements ApplicationListener<NotificationEvent> {

  @Autowired
  private AccountService accountService;

  @Override
  public void onApplicationEvent(NotificationEvent event) {
    accountService.createNotification(event);
  }
}
