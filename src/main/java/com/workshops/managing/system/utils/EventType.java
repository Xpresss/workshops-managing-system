package com.workshops.managing.system.utils;

public enum EventType {
  ALL("ALL"),
  WORKSHOP("WORKSHOP"),
  TRAINING("TRAINING");
  /**
   * Value loaded from .properties file. Should be exactly the same as in DB table event_type.
   */
  private final String typeFromProperties;

  private EventType(String typeFromProperties) {
    this.typeFromProperties = typeFromProperties;
  }

  /**
   *
   * @return Value loaded from .properties file. Should be exactly the same as in DB table event_type.
   */
  public String getTypeFromProperties() {
    return typeFromProperties;
  }
}
