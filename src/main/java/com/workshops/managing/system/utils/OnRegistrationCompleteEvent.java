package com.workshops.managing.system.utils;

import com.workshops.managing.system.model.entity.Account;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

public class OnRegistrationCompleteEvent extends ApplicationEvent {
  private String accountLogin;

  public OnRegistrationCompleteEvent(String login) {
    super(login);
    this.accountLogin = login;
  }

  public OnRegistrationCompleteEvent(
      Account account, Locale locale, String appUrl) {
    super(account);
  }

  public String getAccountLogin() {
    return accountLogin;
  }
  public void setAccountLogin(String accountLogin) {
    this.accountLogin = accountLogin;
  }
}