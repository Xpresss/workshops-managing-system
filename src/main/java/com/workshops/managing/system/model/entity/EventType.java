package com.workshops.managing.system.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class EventType {

  @Id
  @NotNull
  @Column(name = "id", updatable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_type_id_seq")
  @SequenceGenerator(name = "event_type_id_seq", sequenceName = "event_type_id_seq", allocationSize = 1)
  private Long id;

  @NotNull
  @Column(name = "type", updatable = false)
  private String type;

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }
}
