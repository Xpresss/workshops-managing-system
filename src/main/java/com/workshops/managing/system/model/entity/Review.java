package com.workshops.managing.system.model.entity;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Review {

  @Id
  @NotNull
  @Column(name = "id", updatable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_id_seq")
  @SequenceGenerator(name = "event_id_seq", sequenceName = "event_id_seq", allocationSize = 1)
  private Long id;

  @NotNull
  @Column(name = "comment", columnDefinition = "TEXT")
  private String comment;

  @ManyToOne(cascade = CascadeType.REMOVE)
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  private Event event;

  @ManyToOne
  @JoinColumn(name = "account_id", referencedColumnName = "id")
  private Account account;

  @NotNull
  @Column(name = "date")
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
  private LocalDate date;

  public Review() {
  }

  public Review(String comment, Event event, Account account, LocalDate date) {
    this.comment = comment;
    this.event = event;
    this.account = account;
    this.date = date;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public String getComment() {
    return comment;
  }
  public void setComment(String comment) {
    this.comment = comment;
  }

  public Event getEvent() {
    return event;
  }
  public void setEvent(Event event) {
    this.event = event;
  }

  public Account getAccount() {
    return account;
  }
  public void setAccount(Account account) {
    this.account = account;
  }

  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }
}
