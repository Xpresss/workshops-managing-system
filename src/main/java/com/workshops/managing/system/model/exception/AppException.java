package com.workshops.managing.system.model.exception;

import com.workshops.managing.system.service.EventService;
import org.hibernate.StaleObjectStateException;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolationException;
import java.io.Serializable;

public class AppException extends Exception implements Serializable {
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AppException.class);
  public static class Error {
    private String code;

    private String description;

    public Error() {

    }

    public Error(final String code, final String description) {
      this.code = code;
      this.description = description;
    }

    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }
  }

  private Error error;

  public AppException() {

  }

  public AppException(Error error) {
    this.error = error;
    logger.error(toString());
  }

  public Error getError() {
    return error;
  }

  public void setError(Error error) {
    this.error = error;
  }

  public static AppException of(Throwable ex) {
    while (ex.getCause() != null) {
      ex = ex.getCause();
    }
    System.err.println("*** ROOT EXCEPTION: " + ex.toString());
    if (ex instanceof ConstraintViolationException) {
      return of((ConstraintViolationException) ex);
    } else if (ex instanceof StaleObjectStateException) {
      return new AppException(new AppException.Error("Parallel_editing", "Row was updated or deleted by another transaction"));
    } else {
      return new AppException(new AppException.Error("Internal_server_error", ex.getMessage()));
    }
  }

  @Override
  public String toString() {
    StringBuilder message = new StringBuilder();
    message.append(getError().getCode())
        .append(" | ")
        .append(getError().getDescription())
        .append(" ");
    return message.toString();
  }
}
