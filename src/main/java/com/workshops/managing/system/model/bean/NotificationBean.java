package com.workshops.managing.system.model.bean;

import com.workshops.managing.system.model.entity.Notification;

import java.io.Serializable;

public class NotificationBean implements Serializable{

  private Long id;
  private Notification.NotificationType type;
  private Long eventId;

  public NotificationBean() {
  }

  public NotificationBean(Long id, Notification.NotificationType type, Long eventId) {
    this.id = id;
    this.type = type;
    this.eventId = eventId;
  }

  public NotificationBean(Notification notification) {
    this.id = notification.getId();
    this.type = notification.getNotificationType();
    this.eventId = notification.getEvent().getId();
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public Notification.NotificationType getType() {
    return type;
  }
  public void setType(Notification.NotificationType type) {
    this.type = type;
  }

  public Long getEventId() {
    return eventId;
  }
  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  @Override
  public String toString() {
    return "NotificationBean{" +
        "id=" + id +
        ", type=" + type +
        ", eventId=" + eventId +
        '}';
  }
}
