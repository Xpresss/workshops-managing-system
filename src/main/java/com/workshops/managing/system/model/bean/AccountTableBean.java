package com.workshops.managing.system.model.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AccountTableBean implements Serializable{
  private Long draw;
  private Long recordsTotal;
  private Long recordsFiltered;
  private List<String[]> data = new ArrayList<>();
  private String error;

  public Long getDraw() {
    return draw;
  }

  public void setDraw(Long draw) {
    this.draw = draw;
  }

  public Long getRecordsTotal() {
    return recordsTotal;
  }

  public void setRecordsTotal(Long recordsTotal) {
    this.recordsTotal = recordsTotal;
  }

  public Long getRecordsFiltered() {
    return recordsFiltered;
  }

  public void setRecordsFiltered(Long recordsFiltered) {
    this.recordsFiltered = recordsFiltered;
  }

  public List<String[]> getData() {
    return data;
  }

  public void setData(List<String[]> data) {
    this.data = data;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EventTableBean that = (EventTableBean) o;
    return Objects.equals(getDraw(), that.getDraw()) &&
        Objects.equals(getRecordsTotal(), that.getRecordsTotal()) &&
        Objects.equals(getRecordsFiltered(), that.getRecordsFiltered()) &&
        Objects.equals(getData(), that.getData()) &&
        Objects.equals(getError(), that.getError());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getDraw(), getRecordsTotal(), getRecordsFiltered(), getData(), getError());
  }

  @Override
  public String toString() {
    return "EventTableBean{" +
        "draw=" + draw +
        ", recordsTotal=" + recordsTotal +
        ", recordsFiltered=" + recordsFiltered +
        ", data=" + data +
        ", error='" + error + '\'' +
        '}';
  }
}
