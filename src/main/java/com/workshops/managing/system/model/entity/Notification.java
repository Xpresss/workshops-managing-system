package com.workshops.managing.system.model.entity;

import javax.persistence.*;

@Entity
public class Notification {

  public enum NotificationType {
    SIGNUP_ACCEPTED,
    SIGNUP_REJECTED,
    EVENT_EDITED,
    EVENT_ONCOMING,
    NEW_SIGNUP_REQUEST
  }

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_id_seq")
  @SequenceGenerator(name = "notification_id_seq", sequenceName = "notification_id_seq", allocationSize = 1)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
  private Account account;

  @ManyToOne
  @JoinColumn(name = "event_id", referencedColumnName = "id", nullable = false)
  private Event event;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private NotificationType notificationType;

  public Notification() {
  }

  public Notification(Account account, Event event, NotificationType notificationType) {
    this.account = account;
    this.event = event;
    this.notificationType = notificationType;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public Account getAccount() {
    return account;
  }
  public void setAccount(Account account) {
    this.account = account;
  }

  public Event getEvent() {
    return event;
  }
  public void setEvent(Event event) {
    this.event = event;
  }

  public NotificationType getNotificationType() {
    return notificationType;
  }
  public void setNotificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  @Override
  public String toString() {
    return "Notification{" +
        "id=" + id +
        ", account=" + account +
        ", event=" + event +
        ", notificationType=" + notificationType +
        '}';
  }
}
