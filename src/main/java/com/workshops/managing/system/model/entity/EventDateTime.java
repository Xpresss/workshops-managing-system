package com.workshops.managing.system.model.entity;


import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Locale;

@Entity
public class EventDateTime {

  @Id
  @NotNull
  @Column(name = "id", updatable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "event_date_time_seq", sequenceName = "event_date_time_id_seq", allocationSize = 1)
  private Long id;

  @Version
  @NotNull
  @Column(name = "version")
  private Long version;

  @NotNull
  @Column(name = "date")
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
  private LocalDate date;

  @NotNull
  @Column(name = "start_time")
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
  private LocalTime startTime;

  @NotNull
  @Column(name = "end_time")
  @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTime")
  private LocalTime endTime;

  @OneToOne(mappedBy = "eventDateTime", cascade = {CascadeType.REMOVE, CascadeType.MERGE}, fetch = FetchType.LAZY)
  private Event event;

  public EventDateTime() {
  }

  public EventDateTime(LocalDate date, LocalTime startTime, LocalTime endTime) {
    this.date = date;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public EventDateTime(String date, String startTime, String endTime) {
    this.date = LocalDate.parse(date);
    this.startTime = LocalTime.parse(startTime);
    this.endTime = LocalTime.parse(endTime);
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return version;
  }
  public void setVersion(Long version) {
    this.version = version;
  }

  public LocalDate getDate() {
    return date;
  }
  public void setDate(LocalDate date) {
    this.date = date;
  }

  public LocalTime getStartTime() {
    return startTime;
  }
  public void setStartTime(LocalTime startTime) {
    this.startTime = startTime;
  }

  public LocalTime getEndTime() {
    return endTime;
  }
  public void setEndTime(LocalTime endTime) {
    this.endTime = endTime;
  }

  @Override
  public String toString() {
    return this.date.toString("dd MMM, yyyy", Locale.ENGLISH)
        .concat(", ")
        .concat(this.startTime.toString("HH:mm"))
        .concat(" - ")
        .concat(this.endTime.toString("HH:mm"));
  }
}
