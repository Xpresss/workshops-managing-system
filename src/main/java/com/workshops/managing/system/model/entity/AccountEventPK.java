package com.workshops.managing.system.model.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
class AccountEventPK implements Serializable {

    private Long account;

    private Long event;
}