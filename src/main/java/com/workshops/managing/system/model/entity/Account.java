package com.workshops.managing.system.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class Account {

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_seq")
  @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq", allocationSize = 1)
  private Long id;

  @Version
  @NotNull
  @Column(name = "version")
  private Long version;

  @Column(name = "login", nullable = false, unique = true)
  private String login;

  @Column(name = "first_name", nullable = false)
  private String firstName;

  @Column(name = "surname", nullable = false)
  private String surname;

  @Column(name = "email", nullable = false, unique = true)
  private String email;

  @Column(name = "password", nullable = false, length = 60)
  private String password;

  @Column(name = "enabled", nullable = false)
  private boolean enabled;

  @Column(name = "active", nullable = false)
  private boolean active;

  @Column(name = "token_expired", nullable = false)
  private boolean tokenExpired;

  @ManyToOne
  @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
  private Role role;

  @OneToMany(mappedBy = "account")
  private Set<AccountEvent> accountEvents;

  @OneToMany(mappedBy = "account", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER, orphanRemoval = true)
  private Set<Notification> notifications;

  public Account() {
    this.active = false;
    this.enabled = true;
  }

  public Account(String login, String firstName, String surname, String email, String password, boolean enabled, boolean active) {
    this.login = login;
    this.firstName = firstName;
    this.surname = surname;
    this.email = email;
    this.password = password;
    this.enabled = enabled;
    this.active = active;
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return version;
  }
  public void setVersion(Long version) {
    this.version = version;
  }

  public String getLogin() {
    return login;
  }
  public void setLogin(String login) {
    this.login = login;
  }

  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }
  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  public boolean isEnabled() {
    return enabled;
  }
  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isActive() {
    return active;
  }
  public void setActive(boolean active) {
    this.active = active;
  }

  public boolean isTokenExpired() {
    return tokenExpired;
  }
  public void setTokenExpired(boolean tokenExpired) {
    this.tokenExpired = tokenExpired;
  }

  public Role getRole() {
    return role;
  }
  public void setRole(Role role) {
    this.role = role;
  }

  public Set<AccountEvent> getAccountEvents() {
    return accountEvents;
  }
  public void setAccountEvents(Set<AccountEvent> accountEvents) {
    this.accountEvents = accountEvents;
  }

  public Set<Notification> getNotifications() {
    return notifications;
  }
  public void setNotifications(Set<Notification> notifications) {
    this.notifications = notifications;
  }

  @Override
  public String toString() {
    return "Account{" +
        "id=" + id +
        ", version=" + version +
        ", login='" + login + '\'' +
        ", firstName='" + firstName + '\'' +
        ", surname='" + surname + '\'' +
        ", email='" + email + '\'' +
        ", password='" + password + '\'' +
        ", enabled=" + enabled +
        ", active=" + active +
        ", tokenExpired=" + tokenExpired +
        ", role=" + role +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Account account = (Account) o;

    if (enabled != account.enabled) return false;
    if (active != account.active) return false;
    if (tokenExpired != account.tokenExpired) return false;
    if (id != null ? !id.equals(account.id) : account.id != null) return false;
    if (login != null ? !login.equals(account.login) : account.login != null) return false;
    if (firstName != null ? !firstName.equals(account.firstName) : account.firstName != null) return false;
    if (surname != null ? !surname.equals(account.surname) : account.surname != null) return false;
    if (email != null ? !email.equals(account.email) : account.email != null) return false;
    if (password != null ? !password.equals(account.password) : account.password != null) return false;
    return role != null ? role.equals(account.role) : account.role == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (login != null ? login.hashCode() : 0);
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (email != null ? email.hashCode() : 0);
    result = 31 * result + (password != null ? password.hashCode() : 0);
    result = 31 * result + (enabled ? 1 : 0);
    result = 31 * result + (active ? 1 : 0);
    result = 31 * result + (tokenExpired ? 1 : 0);
    result = 31 * result + (role != null ? role.hashCode() : 0);
    return result;
  }
}
