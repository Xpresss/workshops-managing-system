package com.workshops.managing.system.model.bean;

import com.workshops.managing.system.model.entity.Event;
import com.workshops.managing.system.model.entity.EventDateTime;
import com.workshops.managing.system.model.entity.Review;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class EventBean implements Serializable {

  private Long id;
  private String title;
  private String description;
  private String teacher;
  private String place;
  private String date;
  private String startTime;
  private String endTime;
  private String eventType;
  private Integer capacity;
  private Integer signUps;
  private List<ReviewBean> reviews = new ArrayList<>();

  public EventBean() {
  }

  public EventBean(String title, String description, String teacher, String place, String date, String startTime, String endTime) {
    this.title = title;
    this.description = description;
    this.teacher = teacher;
    this.place = place;
    this.date = date;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public EventBean(Long id, String title, String description, String teacher, String place, String date, String startTime, String endTime) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.teacher = teacher;
    this.place = place;
    this.date = date;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public EventBean(Event event, Locale locale) {
    this.id = event.getId();
    this.title = event.getTitle();
    this.description = event.getDescription();
    this.teacher = event.getTeacher();
    this.place = event.getPlace();
    this.eventType = event.getEventType().getType();
    EventDateTime eventDateTime = event.getEventDateTime();
    this.date = eventDateTime.getDate().toString("dd MMM, yyyy", locale);
    this.startTime = eventDateTime.getStartTime().toString("HH:mm");
    this.endTime = eventDateTime.getEndTime().toString("HH:mm");
    this.capacity = event.getCapacity();
    this.signUps = event.getAccountEvents().size();
    Set<Review> reviews = event.getReviews();
    if (reviews != null) {
      reviews.forEach(review -> this.reviews.add(new ReviewBean(review)));
    }
    //    event.getAccountEvents().forEach(accountEvent -> participants.add(new AccountBean(accountEvent.getAccount())));
  }

  public static Event create(EventBean bean) {
    return new Event(bean.getTitle(), bean.getDescription(), bean.getTeacher(), bean.getPlace(), bean.getDate(), bean.getStartTime(), bean.getEndTime(), bean.getCapacity());
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  public String getTeacher() {
    return teacher;
  }
  public void setTeacher(String teacher) {
    this.teacher = teacher;
  }

  public String getPlace() {
    return place;
  }
  public void setPlace(String place) {
    this.place = place;
  }

  public String getDate() {
    return date;
  }
  public void setDate(String date) {
    this.date = date;
  }

  public String getStartTime() {
    return startTime;
  }
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }
  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public String getEventType() {
    return eventType;
  }
  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public Integer getCapacity() {
    return capacity;
  }
  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Integer getSignUps() {
    return signUps;
  }
  public void setSignUps(Integer signups) {
    this.signUps = signups;
  }

  public List<ReviewBean> getReviews() {
    return reviews;
  }
  public void setReviews(List<ReviewBean> reviews) {
    this.reviews = reviews;
  }

  @Override
  public String toString() {
    return "EventBean{" +
        "id=" + id +
        ", title='" + title + '\'' +
        ", description='" + description + '\'' +
        ", teacher='" + teacher + '\'' +
        ", place='" + place + '\'' +
        ", date='" + date + '\'' +
        ", startTime='" + startTime + '\'' +
        ", endTime='" + endTime + '\'' +
        ", eventType='" + eventType + '\'' +
        ", capacity=" + capacity +
        ", signUps=" + signUps +
        ", reviews=" + reviews +
        '}';
  }
}
