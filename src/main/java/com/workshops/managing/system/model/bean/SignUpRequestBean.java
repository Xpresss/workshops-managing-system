package com.workshops.managing.system.model.bean;

import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.model.entity.AccountEvent;
import com.workshops.managing.system.model.entity.Event;

import java.io.Serializable;

public class SignUpRequestBean implements Serializable {

  private Long noAccepted;
  private Integer capacity;
  private Long eventId;
  private String eventName;
  private String eventDate;
  private String userLogin;
  private String userName;
  private String userSurname;

  public SignUpRequestBean() {
  }

  public SignUpRequestBean(AccountEvent accountEvent) {
    Account account = accountEvent.getAccount();
    Event event = accountEvent.getEvent();

    this.eventId = event.getId();
    this.eventName = event.getTitle();
    this.eventDate = event.getEventDateTime().toString();
    this.capacity = event.getCapacity();
    this.noAccepted = event.getAccountEvents().stream().filter(AccountEvent::isConfirmed).count();
    this.userLogin = account.getLogin();
    this.userName = account.getFirstName();
    this.userSurname = account.getSurname();
  }

  public Long getEventId() {
    return eventId;
  }
  public void setEventId(Long eventId) {
    this.eventId = eventId;
  }

  public String getEventName() {
    return eventName;
  }
  public void setEventName(String eventName) {
    this.eventName = eventName;
  }

  public String getEventDate() {
    return eventDate;
  }
  public void setEventDate(String eventDate) {
    this.eventDate = eventDate;
  }

  public String getUserLogin() {
    return userLogin;
  }
  public void setUserLogin(String userLogin) {
    this.userLogin = userLogin;
  }

  public String getUserName() {
    return userName;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserSurname() {
    return userSurname;
  }
  public void setUserSurname(String userSurname) {
    this.userSurname = userSurname;
  }

  public Long getNoAccepted() {
    return noAccepted;
  }
  public void setNoAccepted(Long noAccepted) {
    this.noAccepted = noAccepted;
  }

  public Integer getCapacity() {
    return capacity;
  }
  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }
}
