package com.workshops.managing.system.model.bean;

import com.workshops.managing.system.model.entity.Account;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AccountBean implements Serializable{

//  static final long serialVersionUID = 1L;

  private String login;
  private String password;
  private String role;
  private String firstName;
  private String surname;
  private String email;
  private boolean isEnabled;
  private boolean isActive;
  private List<NotificationBean> notifications = new ArrayList<>();

  public AccountBean() {
  }

  public AccountBean(String login, String firstName, String surname) {
    this.login = login;
    this.firstName = firstName;
    this.surname = surname;
  }

  public AccountBean(Account account) {
    this.login = account.getLogin();
    this.password = account.getPassword();
    this.role = account.getRole().getName();
    this.firstName = account.getFirstName();
    this.surname = account.getSurname();
    this.email = account.getEmail();
    this.isEnabled = account.isEnabled();
    this.isActive = account.isActive();
    account.getNotifications().forEach(notification -> this.notifications.add(new NotificationBean(notification)));
  }

  public AccountBean(String login, String password, String role, String firstName, String surname, String email, boolean isEnabled, boolean isActive) {
    this.login = login;
    this.password = password;
    this.role = role;
    this.firstName = firstName;
    this.surname = surname;
    this.email = email;
    this.isEnabled = isEnabled;
    this.isActive = isActive;
  }

  public Account create(AccountBean bean) {
    Account account = new Account(bean.getLogin(), bean.getFirstName(), bean.getSurname(), bean.getEmail(), bean.getPassword(), bean.isEnabled(), bean.isActive());
    return account;
  }

  public String getLogin() {
    return login;
  }
  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }
  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }
  public void setRole(String role) {
    this.role = role;
  }

  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSurname() {
    return surname;
  }
  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }

  public boolean isEnabled() {
    return isEnabled;
  }
  public void setEnabled(boolean enabled) {
    isEnabled = enabled;
  }

  public boolean isActive() {
    return isActive;
  }
  public void setActive(boolean active) {
    isActive = active;
  }

  public List<NotificationBean> getNotifications() {
    return notifications;
  }
  public void setNotifications(List<NotificationBean> notifications) {
    this.notifications = notifications;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AccountBean bean = (AccountBean) o;

    if (login != null ? !login.equals(bean.login) : bean.login != null) return false;
    if (firstName != null ? !firstName.equals(bean.firstName) : bean.firstName != null) return false;
    return surname != null ? surname.equals(bean.surname) : bean.surname == null;
  }

  @Override
  public int hashCode() {
    int result = login != null ? login.hashCode() : 0;
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    return result;
  }
}
