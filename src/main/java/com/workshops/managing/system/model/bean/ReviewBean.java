package com.workshops.managing.system.model.bean;

import com.workshops.managing.system.model.entity.Review;

import java.io.Serializable;

public class ReviewBean implements Serializable {
  private String comment;
  private String user;
  private String date;

  public ReviewBean() {
  }

  public ReviewBean(String comment, String user, String date) {
    this.comment = comment;
    this.user = user;
    this.date = date;
  }

  public ReviewBean (Review review) {
    this.comment = review.getComment();
    this.user = review.getAccount().getFirstName() + " " + review.getAccount().getSurname();
    this.date = review.getDate().toString();
  }

  public String getComment() {
    return comment;
  }
  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getUser() {
    return user;
  }
  public void setUser(String user) {
    this.user = user;
  }

  public String getDate() {
    return date;
  }
  public void setDate(String date) {
    this.date = date;
  }

  @Override
  public String toString() {
    return "ReviewBean{" +
        "comment='" + comment + '\'' +
        ", user='" + user + '\'' +
        ", date='" + date + '\'' +
        '}';
  }
}
