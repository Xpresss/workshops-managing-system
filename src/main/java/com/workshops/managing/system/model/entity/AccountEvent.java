package com.workshops.managing.system.model.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@IdClass(AccountEventPK.class)
public class AccountEvent implements Serializable {

  @Id
  @ManyToOne(optional = false)
  @JoinColumn(name = "event_id", referencedColumnName = "id")
  private Event event;

  @Id
  @ManyToOne(optional = false)
  @JoinColumn(name = "account_id", referencedColumnName = "id")
  private Account account;

  @Column(name = "is_confirmed", nullable = false)
  private boolean isConfirmed;

  @Version
  @NotNull
  @Column(name = "version")
  private Long version;

  public AccountEvent() {
  }

  public AccountEvent(Event event, Account account) {
    this.event = event;
    this.account = account;
    this.isConfirmed = false;
  }

  public AccountEvent(Event event, Account account, boolean isConfirmed) {
    this.event = event;
    this.account = account;
    this.isConfirmed = isConfirmed;
  }

  public Event getEvent() {
    return event;
  }
  public void setEvent(Event event) {
    this.event = event;
  }

  public Account getAccount() {
    return account;
  }
  public void setAccount(Account account) {
    this.account = account;
  }

  public boolean isConfirmed() {
    return isConfirmed;
  }
  public void setConfirmed(boolean confirmed) {
    isConfirmed = confirmed;
  }

  public Long getVersion() {
    return version;
  }
  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public String toString() {
    return "AccountEvent{" +
        "event=" + event.toString() +
        ", account=" + account.toString() +
        ", isConfirmed=" + isConfirmed +
        ", version=" + version +
        '}';
  }
}
