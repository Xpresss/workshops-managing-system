package com.workshops.managing.system.model.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Event implements Serializable {

  @Id
  @NotNull
  @Column(name = "id", updatable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_id_seq")
  @SequenceGenerator(name = "event_id_seq", sequenceName = "event_id_seq", allocationSize = 1)
  private Long id;

  @Version
  @NotNull
  @Column(name = "version")
  private Long version;

  @NotNull
  @Column(name = "title")
  private String title;

  @NotNull
  @Column(name = "description", columnDefinition = "TEXT")
  private String description;

  @NotNull
  @Column(name = "teacher")
  private String teacher;

  @NotNull
  @Column(name = "place")
  private String place;

  @NotNull
  @Column(name = "capacity")
  private Integer capacity;

  @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
  @JoinColumn(name = "date_time_id", referencedColumnName = "id")
  private EventDateTime eventDateTime;

  @ManyToOne
  @JoinColumn(name = "event_type_id", referencedColumnName = "id")
  private EventType eventType;

  @OneToMany(mappedBy = "event", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<AccountEvent> accountEvents;

  @OneToMany(mappedBy = "event", fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
  private Set<Review> reviews;

  @OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
  private Set<Notification> notifications;

  public Event() {
  }

  public Event(Long id, Long version, String title, String description, String teacher, String place) {
    this.id = id;
    this.version = version;
    this.title = title;
    this.description = description;
    this.teacher = teacher;
    this.place = place;
  }

  public Event(String title, String description, String teacher, String place, String date, String startTime, String endTime, Integer capacity) {
    this.title = title;
    this.description = description;
    this.teacher = teacher;
    this.place = place;
    this.capacity = capacity;
    this.eventDateTime = new EventDateTime(date, startTime, endTime);
  }

  public Long getId() {
    return id;
  }
  public void setId(Long id) {
    this.id = id;
  }

  public Long getVersion() {
    return version;
  }
  public void setVersion(Long version) {
    this.version = version;
  }

  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  public String getTeacher() {
    return teacher;
  }
  public void setTeacher(String teacher) {
    this.teacher = teacher;
  }

  public String getPlace() {
    return place;
  }
  public void setPlace(String place) {
    this.place = place;
  }

  public EventDateTime getEventDateTime() {
    return eventDateTime;
  }
  public void setEventDateTime(EventDateTime eventDateTime) {
    this.eventDateTime = eventDateTime;
  }

  public EventType getEventType() {
    return eventType;
  }
  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public Integer getCapacity() {
    return capacity;
  }
  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public Set<AccountEvent> getAccountEvents() {
    return accountEvents;
  }
  public void setAccountEvents(Set<AccountEvent> accountEvents) {
    this.accountEvents = accountEvents;
  }

  public Set<Review> getReviews() {
    return reviews;
  }
  public void setReviews(Set<Review> reviews) {
    this.reviews = reviews;
  }

  public Set<Notification> getNotifications() {
    return notifications;
  }
  public void setNotifications(Set<Notification> notifications) {
    this.notifications = notifications;
  }

  @Override
  public String toString() {
    return "Event{" +
        "id=" + id +
        ", version=" + version +
        ", title='" + title + '\'' +
        ", description='" + description + '\'' +
        ", teacher='" + teacher + '\'' +
        ", place='" + place + '\'' +
        ", capacity=" + capacity +
        ", eventDateTime=" + eventDateTime +
        ", eventType=" + eventType +
        '}';
  }
}
