package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.EventType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "eventType", path = "eventType")
public interface EventTypeRepository extends CrudRepository<EventType, Long> {

  EventType findByType(@Param("type")String type);
}
