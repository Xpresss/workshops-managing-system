package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.AccountEvent;
import org.joda.time.LocalDate;
import org.springframework.data.repository.CrudRepository;

public interface AccountEventRepository extends CrudRepository<AccountEvent, Long> {
  Iterable<AccountEvent> findAllByIsConfirmedIsFalse();

  AccountEvent findAccountEventByEvent_IdAndAccount_Login(Long eventId, String login);

  Iterable<AccountEvent> findAllByIsConfirmedIsTrueAndAccount_Login(String login);

  Iterable<AccountEvent> findAllByIsConfirmedIsTrueAndAccount_LoginAndEvent_EventDateTimeDateBefore(String login, LocalDate date);

  Iterable<AccountEvent> getAllByEvent_IdAndIsConfirmedIsTrue(Long id);

  Integer countByAccount_LoginAndEvent_Id(String login, Long eventId);
}
