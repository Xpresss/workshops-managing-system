package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends CrudRepository<Role, Integer> {

  Role findRoleByName(@Param("name") String name);
}
