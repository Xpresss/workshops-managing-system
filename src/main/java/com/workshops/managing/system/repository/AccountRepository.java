package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "account", path = "account")
public interface AccountRepository extends CrudRepository<Account, Long> {

  Account findByLoginIgnoreCase(@Param("login") String login);

  Account findByEmail(@Param("email") String email);

  Page<Account> findAllByLoginContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrRole_NameContainingIgnoreCase(
      Pageable pageable,
      @Param("login")String login,
      @Param("firstName") String fistName,
      @Param("surname") String surname,
      @Param("email") String email,
      @Param("role") String role);

  Iterable<Account> findAllByRole_Name(String role);
}
