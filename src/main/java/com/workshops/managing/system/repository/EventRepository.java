package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.Event;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "event", path = "event")
public interface EventRepository extends CrudRepository<Event, Long> {

  Page<Event> findAllByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrTeacherContainingIgnoreCaseOrPlaceContainingIgnoreCase(Pageable pageable,
                                                                                                                                          @Param("title") String title,
                                                                                                                                          @Param("description") String description,
                                                                                                                                          @Param("teacher") String teacher,
                                                                                                                                          @Param("place") String place);

  @Query("SELECT e FROM Event e WHERE e.eventType=:eventType " +
      "AND (UPPER(e.title) LIKE UPPER(CONCAT('%',:title,'%')) " +
      "OR UPPER(e.description) LIKE UPPER(CONCAT('%',:description,'%')) " +
      "OR UPPER(e.teacher) LIKE UPPER(CONCAT('%',:teacher,'%')) " +
      "OR UPPER(e.place) LIKE UPPER(CONCAT('%',:place,'%')))")
  Page<Event> getEventsByTypeUsingFiltersAndSorting(Pageable pageable,
                                                    @Param("eventType") com.workshops.managing.system.model.entity.EventType eventType,
                                                    @Param("title") String title,
                                                    @Param("description") String description,
                                                    @Param("teacher") String teacher,
                                                    @Param("place") String place);

  Iterable<Event> getAllByEventDateTime_DateBetween(LocalDate date1, LocalDate date2);

  Iterable<Event> findAllByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrTeacherContainingIgnoreCaseOrPlaceContainingIgnoreCaseOrEventType_TypeContainingIgnoreCase(String title,
                                                                                                                                                                                  String description,
                                                                                                                                                                                  String teacher,
                                                                                                                                                                                    String place,
                                                                                                                                                                                  String type);
  Iterable<Event> getAllByEventDateTime_DateOrEventDateTime_Date(LocalDate date1, LocalDate date2);
}
