package com.workshops.managing.system.repository;

import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.model.entity.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository
  extends JpaRepository<VerificationToken, Long> {
 
    VerificationToken findByToken(String token);
}