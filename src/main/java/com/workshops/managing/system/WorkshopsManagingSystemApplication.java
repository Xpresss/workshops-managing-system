package com.workshops.managing.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableJpaRepositories("com.workshops.managing.system.repository")
@EntityScan("com.workshops.managing.system.model.entity")
public class WorkshopsManagingSystemApplication {

  public static void main(String[] args) {
    SpringApplication.run(WorkshopsManagingSystemApplication.class, args);
  }
}
