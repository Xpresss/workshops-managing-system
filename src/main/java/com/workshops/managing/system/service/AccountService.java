package com.workshops.managing.system.service;

import com.workshops.managing.system.model.bean.AccountTableBean;
import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.model.entity.Role;
import com.workshops.managing.system.model.entity.VerificationToken;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.utils.NotificationEvent;

public interface AccountService {

  void register(Account account);

  Account getAccountByLogin(String login);

  boolean loginExists(String login);

  boolean emailTaken(String email);

  void createVerificationToken(Account account, String token);

  VerificationToken getVerificationToken(String VerificationToken);

  void activateAccount(Account account);

  VerificationToken generateNewVerificationToken(String token);

  void deleteToken(VerificationToken verificationToken);

  AccountTableBean getAccountsToTable(Long draw, Long order, String dir, int start, int length, String searchValue);

  Iterable<Role> getRoles();

  void editAccount(Account account, String role) throws AppException;

  void createAccount(Account account) throws AppException;

  void enableAccount(String login) throws AppException;

  void disableAccount(String login) throws AppException;

  void createNotification(NotificationEvent notificationEvent);

  void resetPassword(String login, String email) throws AppException;

  void dismissNotification(String login, Long notificationId) throws AppException;
}
