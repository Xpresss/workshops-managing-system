package com.workshops.managing.system.service;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.model.bean.AccountTableBean;
import com.workshops.managing.system.model.entity.*;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.repository.AccountRepository;
import com.workshops.managing.system.repository.RoleRepository;
import com.workshops.managing.system.repository.VerificationTokenRepository;
import com.workshops.managing.system.utils.NotificationEvent;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Component
@Transactional(isolation = Isolation.READ_COMMITTED)
public class AccountServiceImpl implements AccountService {
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AccountService.class);

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private VerificationTokenRepository tokenRepository;

  @Autowired
  private EventService eventService;

  @Autowired
  private JavaMailSender mailSender;

  private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);

  @Value("${role.admin}")
  private String adminRole;

  @Value("${role.hr}")
  private String hrRole;

  @Value("${role.user}")
  private String userRole;

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void register(Account account) {
    account.setRole(roleRepository.findRoleByName(userRole));
    accountRepository.save(account);
    logger.info("Registering account: " + account.toString());
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Account getAccountByLogin(String login) {
    logger.info("Getting account by login: " + login);
    return accountRepository.findByLoginIgnoreCase(login);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public boolean loginExists(String login) {
    logger.info("Checking if account with login: " + login + " already exists");
    return accountRepository.findByLoginIgnoreCase(login) != null;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public boolean emailTaken(String email) {
    logger.info("Checking if account with email address: " + email + " already exists");
    return accountRepository.findByEmail(email) != null;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void createVerificationToken(Account account, String token) {
    VerificationToken myToken = new VerificationToken(token, account);
    logger.info("Creating verification token: " + myToken.toString());
    tokenRepository.save(myToken);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public VerificationToken generateNewVerificationToken(String token) {
    VerificationToken verificationToken = tokenRepository.findByToken(token);
    if (verificationToken != null) {
      tokenRepository.delete(verificationToken);
    }
    VerificationToken newVerificationToken = new VerificationToken(verificationToken.getAccount());
    tokenRepository.save(newVerificationToken);
    logger.info("Creating new verification token: " + verificationToken.toString());
    return newVerificationToken;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void deleteToken(VerificationToken verificationToken) {
    logger.info("Removing verification token: " + verificationToken.toString());
    tokenRepository.delete(verificationToken);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public AccountTableBean getAccountsToTable(Long draw, Long order, String dir, int start, int length, String searchValue) {
    AccountTableBean accountTableBean = new AccountTableBean();
    accountTableBean.setDraw(draw);
    String sortby = "";
    switch (order.toString()) {
      case "0":
        sortby = "login";
        break;

      case "1":
        sortby = "firstName";
        break;

      case "2":
        sortby = "surname";
        break;

      case "3":
        sortby = "email";
        break;

      case "4":
        sortby = "role";
        break;

      default:
        sortby = "login";
        break;

    }
    Boolean isAsc = true;
    if (dir.equals("desc")) {
      isAsc = false;
    }
    Page<Account> page;
    page = accountRepository.findAllByLoginContainingIgnoreCaseOrFirstNameContainingIgnoreCaseOrSurnameContainingIgnoreCaseOrEmailContainingIgnoreCaseOrRole_NameContainingIgnoreCase(
        new PageRequest(start / length, length, isAsc ? Sort.Direction.ASC : Sort.Direction.DESC, sortby),
        searchValue,
        searchValue,
        searchValue,
        searchValue,
        searchValue);

    accountTableBean.setRecordsFiltered(page.getTotalElements());
    accountTableBean.setRecordsTotal(page.getTotalElements());
    for (AccountBean accountBean : page.map(AccountBean::new).getContent()) {
      String[] strings = new String[7];
      strings[0] = accountBean.getLogin();
      strings[1] = accountBean.getFirstName();
      strings[2] = accountBean.getSurname();
      strings[3] = accountBean.getEmail();
      strings[4] = accountBean.getRole();
      strings[5] = String.valueOf(accountBean.isActive());
      strings[6] = String.valueOf(accountBean.isEnabled());
      accountTableBean.getData().add(strings);
    }
    logger.info("Found [" + accountTableBean.getData().size() + "] accounts to table. Searched by filter: " + searchValue + " ,sorted " + (isAsc ? Sort.Direction.ASC.toString() : Sort.Direction.DESC.toString()) + "by: " + sortby);
    return accountTableBean;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<Role> getRoles() {
    logger.info("Getting all user roles from DB");
    return roleRepository.findAll();
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void editAccount(Account account, String roleName) throws AppException {
    Role role = roleRepository.findRoleByName(roleName);
    logger.info("Editing account: " + account.toString());
    if (role == null) {
      throw new AppException(new AppException.Error("Invalid_role", "Selected role does not exist in database"));
    }
    account.setRole(role);
    if (account.getNotifications() == null) {
      account.setNotifications(new HashSet<>());
    }
    accountRepository.save(account);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void createAccount(Account account) throws AppException {
    Role role = roleRepository.findRoleByName(userRole);
    logger.info("Creating account: " + account.toString() + ", by user with ADMIN role");
    if (role == null) {
      throw new AppException(new AppException.Error("DB_Error", "There is no user role in database!\n Try again or contact support."));
    }
    account.setRole(role);
    accountRepository.save(account);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void enableAccount(String login) throws AppException {
    Account account = getAccountByLogin(login);
    if (account == null) {
      throw new AppException(new AppException.Error("Account_not_found", "Account with such login does not exist"));
    }
    logger.info("Enabling account: " + account.toString());
    account.setEnabled(true);
    accountRepository.save(account);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void disableAccount(String login) throws AppException {
    Account account = getAccountByLogin(login);
    if (account == null) {
      throw new AppException(new AppException.Error("Account_not_found", "Account with such login does not exist"));
    }
    logger.info("Disabling account: " + account.toString());
    account.setEnabled(false);
    accountRepository.save(account);
  }

  private void sendEmailNotification(String email) {
    SimpleMailMessage mail = new SimpleMailMessage();
    mail.setTo(email);
    mail.setSubject("Notification from events managing system");
    mail.setText("You have new notification in events managing system. \n");
    mailSender.send(mail);
  }

  @Override
  public void createNotification(NotificationEvent notificationEvent) {
    Event event = eventService.getEventById(notificationEvent.getEventId());
    Account account = getAccountByLogin(notificationEvent.getLogin());

    logger.info("Creating notification " + notificationEvent.getNotificationType().toString() + " for event: " + event.toString() + ", for account: " + account.toString());

    switch (notificationEvent.getNotificationType()) {
      case EVENT_ONCOMING:
      case EVENT_EDITED:
        event.getAccountEvents().forEach(accountEvent -> {
          Account account1 = accountEvent.getAccount();
          account1.getNotifications().add(new Notification(accountEvent.getAccount(), event, notificationEvent.getNotificationType()));
          accountRepository.save(account1);
          sendEmailNotification(account1.getEmail());
        });
        break;

      case SIGNUP_REJECTED:
      case SIGNUP_ACCEPTED:
        account.getNotifications().add(new Notification(account, event, notificationEvent.getNotificationType()));
        accountRepository.save(account);
        sendEmailNotification(account.getEmail());
        break;
      case NEW_SIGNUP_REQUEST:
        accountRepository.findAllByRole_Name(hrRole).forEach(account1 -> {
          account.getNotifications().add(new Notification(account1, event, notificationEvent.getNotificationType()));
          sendEmailNotification(account1.getEmail());
        });
        break;
    }
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void resetPassword(String login, String email) throws AppException {
    logger.info("Resetting password for user with login: " + login + " and email: " + email);
    Account account = accountRepository.findByLoginIgnoreCase(login);
    if (account == null || !account.getEmail().equals(email)) {
      throw new AppException(new AppException.Error("Incorrect_username_or_email", "No such user or incorrect email address"));
    }
    String password = RandomStringUtils.randomAlphanumeric(8);
    account.setPassword(passwordEncoder.encode(password));
    accountRepository.save(account);

    SimpleMailMessage mail = new SimpleMailMessage();
    mail.setTo(account.getEmail());
    mail.setSubject("Reset password");
    mail.setText("You have reset your account's password in workshops and trainings managing system.\n" +
        "new password: " + password +
        "\nLogin to your account and change the password immediately.");
    mailSender.send(mail);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void dismissNotification(String login, Long notificationId) throws AppException {
    logger.info("Dismissing notification with ID: " + notificationId + "by user with login: " + login);
    Account account = accountRepository.findByLoginIgnoreCase(login);
    account.getNotifications().removeIf(notification -> notification.getId().equals(notificationId));
    accountRepository.save(account);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public VerificationToken getVerificationToken(String VerificationToken) {
    logger.info("Getting verification token: " + VerificationToken);
    return tokenRepository.findByToken(VerificationToken);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void activateAccount(Account account) {
    logger.info("Activating account: " + account.toString());
    account.setActive(true);
    accountRepository.save(account);
  }
}
