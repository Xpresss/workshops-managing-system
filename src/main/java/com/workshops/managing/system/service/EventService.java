package com.workshops.managing.system.service;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.model.bean.EventBean;
import com.workshops.managing.system.model.bean.EventTableBean;
import com.workshops.managing.system.model.entity.AccountEvent;
import com.workshops.managing.system.model.entity.Event;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.utils.EventType;

import java.util.Locale;

public interface EventService {

  Iterable<EventBean> getAllEvents(Locale locale);

  void createEvent(Event event, String eventType);

  Event getEventById(Long id);

  void editEvent(Event event, String eventType);

  EventTableBean findRangeWithFilterName(Long draw, Long order, String dir, int start, int length, String searchValue, EventType eventType, Locale locale);

  void removeEvent(Event event);

  void signUpForAnEvent(String login, Long id) throws AppException;

  Iterable<AccountEvent> getAllSignUpRequests();

  void considerSignUpRequest(AccountEvent accountEvent, boolean isConfirmed) throws AppException;

  Iterable<EventBean> getUserEvents(String login, Locale locale) throws AppException;

  Iterable<EventBean> getOncomingEvents(Locale locale) throws AppException;

  Iterable<EventBean> getEventsToRate(String login, Locale locale) throws AppException;

  Iterable<AccountBean> getEventParticipants(Long eventId) throws AppException;

  Iterable<EventBean> searchEvents(Locale locale, String search) throws AppException;

  void removeParticipant(String login, Long eventId) throws AppException;

  void addReview(String review, Long eventId, String login) throws AppException;

  AccountEvent getSignUpRequestToEdit(Long eventId, String login) throws AppException;
}
