package com.workshops.managing.system.service;

import com.workshops.managing.system.model.MyUserPrincipal;
import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {
 
    @Autowired
    private AccountRepository accountRepository;
 
    @Override
    public UserDetails loadUserByUsername(String username) {
        Account account = accountRepository.findByLoginIgnoreCase(username);
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new MyUserPrincipal(account);
    }
}