package com.workshops.managing.system.service;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.model.bean.EventBean;
import com.workshops.managing.system.model.bean.EventTableBean;
import com.workshops.managing.system.model.entity.*;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.repository.AccountEventRepository;
import com.workshops.managing.system.repository.AccountRepository;
import com.workshops.managing.system.repository.EventRepository;
import com.workshops.managing.system.repository.EventTypeRepository;
import com.workshops.managing.system.utils.EventType;
import org.apache.tomcat.jni.Local;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
@Transactional(isolation = Isolation.READ_COMMITTED)
public class EventServiceImpl implements EventService {
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EventService.class);

  @Autowired
  private EventRepository eventRepository;

  @Autowired
  private EventTypeRepository eventTypeRepository;

  @Autowired
  private AccountRepository accountRepository;

  @Autowired
  private AccountEventRepository accountEventRepository;

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Iterable<EventBean> getAllEvents(Locale locale) {
    List<EventBean> eventBeans = new ArrayList<>();
    eventRepository.findAll().forEach(event -> {
      event.setReviews(null);
      eventBeans.add(new EventBean(event, locale));
    });
    logger.info("Getting all events");
    return eventBeans;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void createEvent(Event event, String eventType) {
    event.setEventType(eventTypeRepository.findByType(eventType));
    eventRepository.save(event);
    logger.info("Creating event: " + event.toString());
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Event getEventById(Long id) {
    logger.info("Getting event by id: " + id);
    return eventRepository.findOne(id);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void editEvent(Event event, String eventType) {
    event.setEventType(eventTypeRepository.findByType(eventType));
    logger.info("Editing event: " + event.toString());
    eventRepository.save(event);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public EventTableBean findRangeWithFilterName(Long draw, Long order, String dir, int start, int length, String searchValue, EventType eventType, Locale locale) {
    EventTableBean eventTableBean = new EventTableBean();
    eventTableBean.setDraw(draw);
    String sortby = "";
    switch (order.toString()) {
      case "1":
        sortby = "eventType";
        break;

      case "2":
        sortby = "title";
        break;

      case "4":
        sortby = "teacher";
        break;

      case "5":
        sortby = "place";
        break;

      default:
        sortby = "id";
        break;

    }
    Boolean isAsc = true;
    if (dir.equals("desc")) {
      isAsc = false;
    }
    Page<Event> page;
    if (eventType.equals(EventType.ALL)) {
      page = eventRepository.findAllByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrTeacherContainingIgnoreCaseOrPlaceContainingIgnoreCase(new PageRequest(start / length, length, isAsc ? Sort.Direction.ASC : Sort.Direction.DESC, sortby), searchValue, searchValue, searchValue, searchValue);
    } else {
      page = eventRepository.getEventsByTypeUsingFiltersAndSorting(new PageRequest(start / length, length, isAsc ? Sort.Direction.ASC : Sort.Direction.DESC, sortby), eventTypeRepository.findByType(eventType.getTypeFromProperties()), searchValue, searchValue, searchValue, searchValue);
    }
    eventTableBean.setRecordsFiltered(page.getTotalElements());
    eventTableBean.setRecordsTotal(page.getTotalElements());
//    for (EventBean eventBean : page.map(EventBean::new).getContent()) {
    for (EventBean eventBean : page.map(event -> new EventBean(event, locale)).getContent()) {
      String[] strings = new String[8];
      strings[0] = eventBean.getId().toString();
      strings[1] = eventBean.getEventType();
      strings[2] = eventBean.getTitle();
      strings[3] = eventBean.getDescription();
      strings[4] = eventBean.getTeacher();
      strings[5] = eventBean.getPlace();
      strings[6] = eventBean.getDate() + ", " + eventBean.getStartTime() + " - " + eventBean.getEndTime();
      strings[7] = eventBean.getCapacity().toString();
      eventTableBean.getData().add(strings);
    }
    logger.info("Found [" + eventTableBean.getData().size() + "] events to table. Searched by filter: " + searchValue + " and event type: " + eventType.toString() + " ,sorted " + (isAsc ? Sort.Direction.ASC.toString() : Sort.Direction.DESC.toString()) + "by: " + sortby);
    return eventTableBean;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void removeEvent(Event event) {
    event.getNotifications().forEach(notification -> {
      Account account = notification.getAccount();
      account.getNotifications().remove(notification);
      accountRepository.save(account);
    });
    logger.info("Removing event: " + event.toString());
    eventRepository.delete(event);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void signUpForAnEvent(String login, Long eventId) throws AppException {
    Account account = accountRepository.findByLoginIgnoreCase(login);
    Event event = eventRepository.findOne(eventId);
    if (event == null) {
      throw new AppException(new AppException.Error("Event_not_found", "Chosen event does not exist anymore"));
    }
    if (event.getAccountEvents().size() >= event.getCapacity()) {
      throw new AppException(new AppException.Error("Event_is_full", "There is no more place on this event."));
    }

    int comparator = event.getEventDateTime().getDate().compareTo(new LocalDate());
    if (comparator < 0) {
      throw new AppException(new AppException.Error("Too_late", "This event has already been started"));
    }
    if (comparator == 0) {
      if (event.getEventDateTime().getEndTime().compareTo(new LocalTime()) < 0) {
        throw new AppException(new AppException.Error("Too_late", "This event has already been started"));
      }
    }

    AccountEvent accountEvent = new AccountEvent(event, account);
    event.getAccountEvents().add(accountEvent);

    logger.info("Signing up user: " + account.toString() + " , for event: " + event.toString());
    eventRepository.save(event);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Iterable<AccountEvent> getAllSignUpRequests() {
    logger.info("Getting all sign up requests");
    return accountEventRepository.findAllByIsConfirmedIsFalse();
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void considerSignUpRequest(AccountEvent accountEvent, boolean isConfirmed) throws AppException {
    if (accountEvent.getEvent().getAccountEvents().stream().filter(AccountEvent::isConfirmed).count() >= accountEvent.getEvent().getCapacity()) {
      throw new AppException(new AppException.Error("Event_is_full", "There is no more place on this event."));
    }
    if (isConfirmed) {
      accountEvent.setConfirmed(true);
      if (accountEventRepository.countByAccount_LoginAndEvent_Id(accountEvent.getAccount().getLogin(), accountEvent.getEvent().getId()) != 1) {
        throw new AppException(new AppException.Error("Parallel_editing", "Sign up request has been rejected already"));
      }
      accountEventRepository.save(accountEvent);
    } else {
      accountEvent.setConfirmed(false);
      accountEventRepository.save(accountEvent);
      Event event = accountEvent.getEvent();
      event.getAccountEvents().remove(accountEvent);
      eventRepository.save(event);
    }
    logger.info(isConfirmed ? "Accepting " : "Rejecting " + accountEvent.getAccount().toString() + " sign up request for event:" + accountEvent.getEvent().toString());
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<EventBean> getUserEvents(String login, Locale locale) throws AppException {
    logger.info("Getting all events for user: " + login);
    List<EventBean> eventBeanList = new ArrayList<>();
    accountEventRepository.findAllByIsConfirmedIsTrueAndAccount_Login(login).forEach(accountEvent -> eventBeanList.add(new EventBean(accountEvent.getEvent(), locale)));
    return eventBeanList;
  }

  private Iterable<Event> getEventsForNotifications() {
    logger.info("Getting events for oncoming notification. (Date from: " + new LocalDate().toString() + " to: " + new LocalDate().plusDays(7).toString());
    return eventRepository.getAllByEventDateTime_DateOrEventDateTime_Date(new LocalDate(), new LocalDate().plusDays(7));
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<EventBean> getOncomingEvents(Locale locale) throws AppException {
    logger.info("Getting oncoming events");
    List<EventBean> eventBeans = new ArrayList<>();
    eventRepository.getAllByEventDateTime_DateBetween(new LocalDate(), new LocalDate().plusDays(7)).forEach(event -> eventBeans.add(new EventBean(event, locale)));
    return eventBeans;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<EventBean> getEventsToRate(String login, Locale locale) throws AppException {
    logger.info("Getting events to rate for user: " + login);
    List<EventBean> eventBeans = new ArrayList<>();
    accountEventRepository.findAllByIsConfirmedIsTrueAndAccount_LoginAndEvent_EventDateTimeDateBefore(login, new LocalDate()).forEach(accountEvent -> eventBeans.add(new EventBean(accountEvent.getEvent(), locale)));
    return eventBeans;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<AccountBean> getEventParticipants(Long eventId) throws AppException {
    logger.info("Getting participants for event with ID: " + eventId);
    List<AccountBean> accountBeans = new ArrayList<>();
    accountEventRepository.getAllByEvent_IdAndIsConfirmedIsTrue(eventId).forEach(accountEvent -> accountBeans.add(new AccountBean(accountEvent.getAccount().getLogin(), accountEvent.getAccount().getFirstName(), accountEvent.getAccount().getSurname())));
    return accountBeans;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Iterable<EventBean> searchEvents(Locale locale, String search) throws AppException {
    logger.info("Searching events with phrase: " + search);
    List<EventBean> eventBeans = new ArrayList<>();
    eventRepository.findAllByTitleContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrTeacherContainingIgnoreCaseOrPlaceContainingIgnoreCaseOrEventType_TypeContainingIgnoreCase(search, search, search, search, search)
        .forEach(event -> eventBeans.add(new EventBean(event, locale)));
    return eventBeans;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void removeParticipant(String login, Long eventId) throws AppException {
    Event event = eventRepository.findOne(eventId);
    if (event == null) {
      throw new AppException(new AppException.Error("Event_not_found", "Such event does not exist anymore"));
    }
    try {
      event.getAccountEvents().remove(accountEventRepository.findAccountEventByEvent_IdAndAccount_Login(eventId, login));
    } catch (NullPointerException e) {
      throw new AppException(new AppException.Error("Participant_not_found", "Such participant does not exist"));
    }
    logger.info("Removing user with login: " + login + " from event: " + event.toString());
    eventRepository.save(event);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public void addReview(String review, Long eventId, String login) throws AppException {
    Event event = eventRepository.findOne(eventId);
    Account account = accountRepository.findByLoginIgnoreCase(login);
    if (event == null) {
      throw new AppException(new AppException.Error("Event_not_found", "Such event does not exist anymore"));
    }
    if (account == null) {
      throw new AppException(new AppException.Error("Account_not_found", "There has been problem with your account"));
    }
    for (Review review1 : event.getReviews()) {
      if (review1.getAccount().equals(account)) {
        throw new AppException(new AppException.Error("Already_reviewed", "You have reviewed this event already"));
      }
    }

    int comparator = event.getEventDateTime().getDate().compareTo(new LocalDate());
    if (comparator > 0) {
      throw new AppException(new AppException.Error("Can_not_comment", "This event did not take place yet"));
    }
    if (comparator == 0) {
      if (event.getEventDateTime().getEndTime().compareTo(new LocalTime()) > 0) {
        throw new AppException(new AppException.Error("Can_not_comment", "This event did not take place yet"));
      }
    }

    List<AccountBean> participants = (List<AccountBean>) getEventParticipants(eventId);
    if (!participants.contains(new AccountBean(account.getLogin(), account.getFirstName(), account.getSurname()))) {
      throw new AppException(new AppException.Error("Not_participant", "Only participants can review events"));
    }
    event.getReviews().add(new Review(review, event, account, new LocalDate()));
    logger.info("Adding review: (" + review + ") to event: " + event.toString());
    eventRepository.save(event);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public AccountEvent getSignUpRequestToEdit(Long eventId, String login) throws AppException {
    logger.info("Getting sign up request for user with login: " + login + " ,for event with ID: " + eventId + " for edition");
    AccountEvent accountEvent = accountEventRepository.findAccountEventByEvent_IdAndAccount_Login(eventId, login);
    if (accountEvent == null) {
      throw new AppException(new AppException.Error("Request_not_found", "Chosen sign up request does not exist anymore"));
    }
    return accountEvent;
  }

  /**
   * Every day at 00:00
   */
  @Scheduled(cron = "0 0 0 * * ?")
  public void sendNotificationsForOncomingEvents() {
    logger.info("Sending notifications for oncoming events");
    getEventsForNotifications().forEach(event -> event.getAccountEvents().forEach(accountEvent -> {
      Account account = accountEvent.getAccount();
      account.getNotifications().add(new Notification(account, event, Notification.NotificationType.EVENT_ONCOMING));
      accountRepository.save(account);
    }));
  }
}