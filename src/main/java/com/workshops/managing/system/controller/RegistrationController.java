package com.workshops.managing.system.controller;

import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.model.entity.VerificationToken;
import com.workshops.managing.system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Locale;

@RestController
public class RegistrationController {

  @Autowired
  private AccountService accountService;

  @Autowired
  private JavaMailSender mailSender;

  @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
  public ModelAndView confirmRegistration
      (WebRequest request, Model model, @RequestParam("token") String token) {

    Locale locale = request.getLocale();
    ModelAndView modelAndView = new ModelAndView();

    VerificationToken verificationToken = accountService.getVerificationToken(token);
    if (verificationToken == null) {
      modelAndView.setViewName("invalidToken");
      modelAndView.addObject("token", token);
      return modelAndView;
    }

    Account account = verificationToken.getAccount();
    Calendar cal = Calendar.getInstance();
    if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
      modelAndView.setViewName("tokenExpired");
      modelAndView.addObject("token", token);
      return modelAndView;
    }

    accountService.activateAccount(account);
    modelAndView.setViewName("login");
    modelAndView.addObject("activated", true);
    accountService.deleteToken(verificationToken);
    return modelAndView;
  }

  @RequestMapping(value = "/resendRegistrationToken", method = RequestMethod.GET)
  @ResponseBody
  public ModelAndView resendRegistrationToken(HttpServletRequest request, @RequestParam("token") String existingToken) {

    VerificationToken newToken = accountService.generateNewVerificationToken(existingToken);

    String appUrl =
        "http://" + request.getServerName() +
            ":" + request.getServerPort() +
            request.getContextPath();
    SimpleMailMessage email =
        constructResendVerificationTokenEmail(appUrl, newToken.getToken(),  newToken.getAccount().getEmail());
    mailSender.send(email);

    return (new ModelAndView("home"));
  }


  private SimpleMailMessage constructResendVerificationTokenEmail
      (String contextPath, String token, String emailAddress) {
    String confirmationUrl =
        contextPath + "/registrationConfirm.html?token=" + token;
    SimpleMailMessage email = new SimpleMailMessage();
    email.setSubject("Resend Registration Token");
    email.setText("Welcome to workshops and trainings managing system.\n" +
        "Before you login to our site, please verify your email address.\n" +
        "Click this link to complete verification process." + confirmationUrl);
    email.setTo(emailAddress);
    return email;
  }
}
