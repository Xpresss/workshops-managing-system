package com.workshops.managing.system.controller;

import com.workshops.managing.system.model.bean.EventBean;
import com.workshops.managing.system.model.bean.EventTableBean;
import com.workshops.managing.system.model.bean.SignUpRequestBean;
import com.workshops.managing.system.model.entity.AccountEvent;
import com.workshops.managing.system.model.entity.Event;
import com.workshops.managing.system.model.entity.EventDateTime;
import com.workshops.managing.system.model.entity.Notification;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.service.EventService;
import com.workshops.managing.system.utils.EventType;
import com.workshops.managing.system.utils.NotificationEvent;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.workshops.managing.system.controller.SecurityController.getCurrentLogin;

@RestController
@RequestMapping(value = "events/")
@SessionAttributes({"event", "accountEvent"})
public class EventController {

  @Autowired
  private EventService eventService;

  @Autowired
  ApplicationEventPublisher eventPublisher;

  @Value("${eventType.workshop}")
  private String workshop;
  @Value("${eventType.training}")
  private String training;

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "getAllEventsToTable")
  public EventTableBean getAllEventsToTable(
      @RequestParam("draw") Long draw,
      @RequestParam("order[0][column]") Long order,
      @RequestParam("order[0][dir]") String dir,
      @RequestParam("start") int start,
      @RequestParam("length") int length,
      @RequestParam("search[value]") String searchValue, HttpServletRequest request) {
    return eventService.findRangeWithFilterName(draw, order, dir, start, length, searchValue, EventType.ALL, request.getLocale());
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "getOnlyWorkshopsToTable")
  public EventTableBean getOnlyWorkshopsToTable(
      @RequestParam("draw") Long draw,
      @RequestParam("order[0][column]") Long order,
      @RequestParam("order[0][dir]") String dir,
      @RequestParam("start") int start,
      @RequestParam("length") int length,
      @RequestParam("search[value]") String searchValue, HttpServletRequest request) {
    return eventService.findRangeWithFilterName(draw, order, dir, start, length, searchValue, EventType.WORKSHOP, request.getLocale());
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "getOnlyTrainingsToTable")
  public EventTableBean getOnlyTrainingsToTable(
      @RequestParam("draw") Long draw,
      @RequestParam("order[0][column]") Long order,
      @RequestParam("order[0][dir]") String dir,
      @RequestParam("start") int start,
      @RequestParam("length") int length,
      @RequestParam("search[value]") String searchValue, HttpServletRequest request) {
    return eventService.findRangeWithFilterName(draw, order, dir, start, length, searchValue, EventType.TRAINING, request.getLocale());
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "get")
  public Iterable<EventBean> getAllEvents(HttpServletRequest request) {
    return eventService.getAllEvents(request.getLocale());
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "search")
  public Iterable<EventBean> filterEvents(@RequestParam String filter, HttpServletRequest request) {
    try {
      return eventService.searchEvents(request.getLocale(), filter);
    } catch (AppException e) {
      e.printStackTrace();
      return null;
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createEvent(@RequestBody EventBean eventBean) {
    try {
    /* -- Validation -- */
      if (eventBean.getTitle() == null || Objects.equals(eventBean.getTitle(), "")) {
        throw new AppException(new AppException.Error("Validation_error15", "Event title can not be empty!"));
      }
      if (eventBean.getTitle().length() < 5) {
        throw new AppException(new AppException.Error("Validation_error16", "Event's title minimum length is 5 characters!"));
      }
      if (eventBean.getEventType() == null || Objects.equals(eventBean.getEventType(), "")) {
        throw new AppException(new AppException.Error("Validation_error17", "Event type can not be empty!"));
      }
      if (!eventBean.getEventType().equals(workshop) && !eventBean.getEventType().equals(training)) {
        throw new AppException(new AppException.Error("Validation_error18", "Event type must be " + workshop + " or " + training + "!"));
      }
      if (eventBean.getTeacher() == null || Objects.equals(eventBean.getTeacher(), "")) {
        throw new AppException(new AppException.Error("Validation_error19", "Teacher is required!"));
      }
      if (eventBean.getPlace() == null || Objects.equals(eventBean.getPlace(), "")) {
        throw new AppException(new AppException.Error("Validation_error20", "Event's place is required!"));
      }
      if (eventBean.getCapacity() == null) {
        throw new AppException(new AppException.Error("Validation_error21", "Event's person limit is required!"));
      }
      if (eventBean.getCapacity() < 0 || eventBean.getCapacity() > 100) {
        throw new AppException(new AppException.Error("Validation_error22", "Event's person limit must be grater than 0 and less than 100!"));
      }
      if (eventBean.getDate() == null) {
        throw new AppException(new AppException.Error("Validation_error23", "Event's date is required!"));
      }
      if (eventBean.getDate().compareTo(String.valueOf(LocalDate.now())) < 0) {
        throw new AppException(new AppException.Error("Validation_error24", "Event's date can not be from the past!"));
      }
      if (eventBean.getStartTime() == null) {
        throw new AppException(new AppException.Error("Validation_error25", "Event's start time is required!"));
      }
      if (eventBean.getEndTime() == null) {
        throw new AppException(new AppException.Error("Validation_error26", "Event's end time is required!"));
      }
      if (eventBean.getStartTime().compareTo(eventBean.getEndTime()) >= 0) {
        throw new AppException(new AppException.Error("Validation_error27", "End time must be after start time!"));
      }
      if (LocalDateTime.now().toString().compareTo(eventBean.getDate() + "T" + eventBean.getStartTime()) > 0) {
        throw new AppException(new AppException.Error("Validation_error28", "Event start time can not be from the past!"));
      }
      if (eventBean.getDescription() == null || eventBean.getDescription().equals("")) {
        throw new AppException(new AppException.Error("Validation_error29", "Event's description is required!"));
      }
      if (eventBean.getDescription().length() < 10) {
        throw new AppException(new AppException.Error("Validation_error30", "Event's description must contain at least 10 characters!"));
      }
    /* -- Validation -- */

      eventService.createEvent(EventBean.create(eventBean), eventBean.getEventType());
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "getToEdit/{id}")
  public ResponseEntity<?> getEventToEdit(HttpServletRequest httpRequest, Model model, @PathVariable Long id) {
    try {
      Event eventToEdit = eventService.getEventById(id);
      if (eventToEdit == null) {
        throw new AppException(new AppException.Error("Event_not_found", "Chosen event does not exist anymore"));
      }
      model.addAttribute("event", eventToEdit);
      EventBean bean = new EventBean(eventToEdit, httpRequest.getLocale());
      bean.setDate(eventToEdit.getEventDateTime().getDate().toString("dd MMMM, yyyy", httpRequest.getLocale()));
      return new ResponseEntity<>(bean, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      e.printStackTrace();
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "sign-up-requests/to-edit")
  public ResponseEntity<?> getSignUpRequestToEdit(Model model, @RequestParam Long eventId, @RequestParam String login) {
    try {
      AccountEvent accountEvent = eventService.getSignUpRequestToEdit(eventId, login);
      model.addAttribute("accountEvent", accountEvent);
      return new ResponseEntity<>(accountEvent.isConfirmed(), HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "edit/{id}", method = RequestMethod.PUT)
  public ResponseEntity<?> editEvent(@ModelAttribute Event event, @PathVariable Long id, @RequestBody EventBean eventBean) {
    try {
      if (eventBean.getId() != null && eventService.getEventById(eventBean.getId()) == null) {
        throw new AppException(new AppException.Error("Internal_server_error", ""));
      }

          /* -- Validation -- */
      if (eventBean.getTitle() == null || Objects.equals(eventBean.getTitle(), "")) {
        throw new AppException(new AppException.Error("Validation_error15", "Event title can not be empty!"));
      }
      if (eventBean.getTitle().length() < 5) {
        throw new AppException(new AppException.Error("Validation_error16", "Event's title minimum length is 5 characters!"));
      }
      if (eventBean.getEventType() == null || Objects.equals(eventBean.getEventType(), "")) {
        throw new AppException(new AppException.Error("Validation_error17", "Event type can not be empty!"));
      }
      if (!eventBean.getEventType().equals(workshop) && !eventBean.getEventType().equals(training)) {
        throw new AppException(new AppException.Error("Validation_error18", "Event type must be " + workshop + " or " + training + "!"));
      }
      if (eventBean.getTeacher() == null || Objects.equals(eventBean.getTeacher(), "")) {
        throw new AppException(new AppException.Error("Validation_error19", "Teacher is required!"));
      }
      if (eventBean.getPlace() == null || Objects.equals(eventBean.getPlace(), "")) {
        throw new AppException(new AppException.Error("Validation_error20", "Event's place is required!"));
      }
      if (eventBean.getCapacity() == null) {
        throw new AppException(new AppException.Error("Validation_error21", "Event's person limit is required!"));
      }
      if (eventBean.getCapacity() < 0 || eventBean.getCapacity() > 100) {
        throw new AppException(new AppException.Error("Validation_error22", "Event's person limit must be grater than 0 and less than 100!"));
      }
      if (eventBean.getCapacity() < event.getAccountEvents().stream().filter(AccountEvent::isConfirmed).count()) {
        throw new AppException(new AppException.Error("Validation_error31", "Event's person limit can not be smaller than number of participants!"));
      }
      if (eventBean.getCapacity() < event.getAccountEvents().size()) {
        throw new AppException(new AppException.Error("Validation_error32", "Event's person limit can not be smaller than number of participants and sign up requests!"));
      }
      if (eventBean.getDate() == null) {
        throw new AppException(new AppException.Error("Validation_error23", "Event's date is required!"));
      }
      if (eventBean.getDate().compareTo(String.valueOf(LocalDate.now())) < 0) {
        throw new AppException(new AppException.Error("Validation_error24", "Event's date can not be from the past!"));
      }
      if (eventBean.getStartTime() == null) {
        throw new AppException(new AppException.Error("Validation_error25", "Event's start time is required!"));
      }
      if (eventBean.getEndTime() == null) {
        throw new AppException(new AppException.Error("Validation_error26", "Event's end time is required!"));
      }
      if (eventBean.getStartTime().compareTo(eventBean.getEndTime()) >= 0) {
        throw new AppException(new AppException.Error("Validation_error27", "End time must be after start time!"));
      }
      if (LocalDateTime.now().toString().compareTo(eventBean.getDate() + "T" + eventBean.getStartTime()) > 0) {
        throw new AppException(new AppException.Error("Validation_error28", "Event start time can not be from the past!"));
      }
      if (eventBean.getDescription() == null || eventBean.getDescription().equals("")) {
        throw new AppException(new AppException.Error("Validation_error29", "Event's description is required!"));
      }
      if (eventBean.getDescription().length() < 10) {
        throw new AppException(new AppException.Error("Validation_error30", "Event's description must contain at least 10 characters!"));
      }
    /* -- Validation -- */

      event.setDescription(eventBean.getDescription());
      event.setPlace(eventBean.getPlace());
      event.setTeacher(eventBean.getTeacher());
      event.setTitle(eventBean.getTitle());
      event.setCapacity(eventBean.getCapacity());
      EventDateTime eventDateTime = event.getEventDateTime();
      eventDateTime.setDate(LocalDate.parse(eventBean.getDate()));
      eventDateTime.setStartTime(LocalTime.parse(eventBean.getStartTime()));
      eventDateTime.setEndTime(LocalTime.parse(eventBean.getEndTime()));
      eventService.editEvent(event, eventBean.getEventType());
      eventPublisher.publishEvent(new NotificationEvent(getCurrentLogin(), event.getId(), Notification.NotificationType.EVENT_EDITED));
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "remove/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> removeEvent(@ModelAttribute Event event, @PathVariable Long id) {
    try {
      if (!event.getId().equals(id)) {
        throw new AppException(new AppException.Error("Internal_server_error", ""));
      }
      eventService.removeEvent(event);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      e.printStackTrace();
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "sign-up", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AppException> signUpForAnEvent(@RequestParam Long eventId, @RequestParam String login) {
    try {
      if (!login.equals(getCurrentLogin())) {
        throw new AppException(new AppException.Error("Login_mismatch","You can not sign up as other user"));
      }
      eventService.signUpForAnEvent(login, eventId);
      eventPublisher.publishEvent(new NotificationEvent(login, eventId, Notification.NotificationType.NEW_SIGNUP_REQUEST));
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (InvalidDataAccessApiUsageException e) {
      return new ResponseEntity<>(new AppException(new AppException.Error("Double_sign_up", "You have already signed up for this event.")), HttpStatus.CONFLICT);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "sign-up-requests/get")
  public ResponseEntity<Object> getSignUpRequests() {
    try {
      List<SignUpRequestBean> signUpRequestBeans = new ArrayList<>();

      eventService.getAllSignUpRequests().forEach(accountEvent -> signUpRequestBeans.add(new SignUpRequestBean(accountEvent)));
      return new ResponseEntity<>(signUpRequestBeans, HttpStatus.OK);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "sign-up-requests/consider", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody
  ResponseEntity<?> considerSignUpRequest(@ModelAttribute AccountEvent accountEvent, @RequestParam("eventId") Long eventId, @RequestParam String login, @RequestParam("isConfirmed") boolean isConfirmed) {
    HttpHeaders headers = new HttpHeaders();
    try {
      headers.setContentType(MediaType.TEXT_PLAIN);
      eventService.considerSignUpRequest(accountEvent, isConfirmed);
      eventPublisher.publishEvent(new NotificationEvent(login, eventId, isConfirmed ? Notification.NotificationType.SIGNUP_ACCEPTED : Notification.NotificationType.SIGNUP_REJECTED));
      return new ResponseEntity<>(isConfirmed ? "Request accepted successfully." : "Request rejected successfully.", headers, HttpStatus.OK);
    } catch (AppException e) {
      headers.setContentType(MediaType.APPLICATION_JSON);
      return new ResponseEntity<>(e, headers, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      headers.setContentType(MediaType.APPLICATION_JSON);
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, headers, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "/userEvents")
  public @ResponseBody
  ResponseEntity<?> usersEventsByLogin(@RequestParam String login, HttpServletRequest request) {
    try {
      if (!login.equals(getCurrentLogin())) {
        return new ResponseEntity<>(new AppException(new AppException.Error("Security_exception", "There has been some problems with authentication.")), HttpStatus.UNAUTHORIZED);
      }
      return new ResponseEntity<>(eventService.getUserEvents(login, request.getLocale()), HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @ResponseBody
  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "/oncomingEvents")
  public ResponseEntity<?> oncomingEvents(HttpServletRequest request) {
    try {
      return new ResponseEntity<>(eventService.getOncomingEvents(request.getLocale()), HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @ResponseBody
  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "toRate")
  public ResponseEntity<?> getEventsToRate(@RequestParam String login, HttpServletRequest request) {
    try {
      if (!login.equals(getCurrentLogin())) {
        return new ResponseEntity<>(new AppException(new AppException.Error("Security_exception", "There has been some problems with authentication.")), HttpStatus.UNAUTHORIZED);
      }
      return new ResponseEntity<>(eventService.getEventsToRate(login, request.getLocale()), HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @ResponseBody
  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "participants")
  public ResponseEntity<?> getEventParticipants(@RequestParam Long eventId) {
    try {
      return new ResponseEntity<>(eventService.getEventParticipants(eventId), HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('HR')")
  @RequestMapping(value = "removeParticipant")
  public ResponseEntity<?> removeParticipant(@RequestParam String login, @RequestParam Long eventId) {
    try {
      eventService.removeParticipant(login, eventId);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "event")
  public EventBean getSingleEvent(HttpServletRequest request, @RequestParam Long id) {
    return new EventBean(eventService.getEventById(id), request.getLocale());
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "addReview")
  public ResponseEntity<?> addReview(@RequestBody String review, @RequestParam Long eventId) {
    try {
      if (review == null || review.isEmpty()) {
        throw new AppException(new AppException.Error("Validation_error33", "Can not add empty review"));
      }
      eventService.addReview(review, eventId, getCurrentLogin());
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }

  }

  private HttpHeaders httpHeaders;

  @PostConstruct
  private HttpHeaders initializeHttpHeaders() {
    httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return httpHeaders;
  }
}
