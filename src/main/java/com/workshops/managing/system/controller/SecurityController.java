package com.workshops.managing.system.controller;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class SecurityController {

  @Autowired
  private AccountService accountService;

  @RequestMapping(value = "/security/account", method = RequestMethod.GET)
  public @ResponseBody
  AccountBean getUserAccount(HttpServletRequest httpServletRequest) {
    Account account = accountService.getAccountByLogin(getCurrentLogin());
    account.setPassword(null);
    return new AccountBean(account);
  }

  public static String getCurrentLogin() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();

    UserDetails springSecurityUser = null;
    String userName = null;

    if(authentication != null) {
      if (authentication.getPrincipal() instanceof UserDetails) {
        springSecurityUser = (UserDetails) authentication.getPrincipal();
        userName = springSecurityUser.getUsername();
      } else if (authentication.getPrincipal() instanceof String) {
        userName = (String) authentication.getPrincipal();
      }
    }

    return userName;
  }
}