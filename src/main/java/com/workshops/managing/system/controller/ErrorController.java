package com.workshops.managing.system.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {

  @RequestMapping(value = "/error")
  public ModelAndView renderErrorPage(HttpServletRequest httpRequest) {
    ModelAndView errorPage = new ModelAndView();

    switch(getErrorCode(httpRequest)) {
      case 404: {
        errorPage.setViewName("404");
        break;
      }
      case 403: {
        errorPage.setViewName("403");
        break;
      }
      case 500: {
        errorPage.setViewName("500");
        break;
      }
    }
    return errorPage;
  }

  private int getErrorCode(HttpServletRequest httpRequest) {
    return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
  }

  @Override
  public String getErrorPath() {
    return "/error";
  }
}
