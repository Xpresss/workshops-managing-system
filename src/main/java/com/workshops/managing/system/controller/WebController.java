package com.workshops.managing.system.controller;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static com.workshops.managing.system.controller.SecurityController.getCurrentLogin;

@Controller
public class WebController {

  @Autowired
  private AccountService accountService;

  @Autowired
  private EventController eventController;

  @RequestMapping("/")
  public ModelAndView home(ModelAndView modelAndView) {
    modelAndView.setViewName("home");
    return modelAndView;
  }

  @RequestMapping("/hr/events")
  public ModelAndView events(ModelAndView modelAndView) {
    modelAndView.setViewName("hr/events");
    return modelAndView;
  }

  @RequestMapping("/eventModal")
  public ModelAndView eventModal(ModelAndView modelAndView) {
    modelAndView.setViewName("eventModal");
    return modelAndView;
  }

  @RequestMapping("/hr/create-event")
  public ModelAndView eventCreate(ModelAndView modelAndView) {
    modelAndView.setViewName("hr/eventCreate");
    return modelAndView;
  }

  @RequestMapping("/login")
  public ModelAndView login(ModelAndView modelAndView) {
    modelAndView.setViewName("login");
    modelAndView.addObject("activated", false);
    return modelAndView;
  }

  @RequestMapping("/register")
  public ModelAndView register(ModelAndView modelAndView) {
    modelAndView.setViewName("register");
    return modelAndView;
  }

  @RequestMapping("/forgot-password")
  public ModelAndView forgotPassword(ModelAndView modelAndView) {
    modelAndView.setViewName("forgotPassword");
    return modelAndView;
  }

  @RequestMapping("/errorPage")
  public ModelAndView errorPage(ModelAndView modelAndView) {
    modelAndView.setViewName("errorPage");
    return modelAndView;
  }

  @RequestMapping("/invalidToken")
  public ModelAndView invalidToken(ModelAndView modelAndView) {
    modelAndView.setViewName("invalidToken");
    return modelAndView;
  }

  @RequestMapping("/tokenExpired")
  public ModelAndView tokenExpired(ModelAndView modelAndView) {
    modelAndView.setViewName("tokenExpired");
    return modelAndView;
  }

  @RequestMapping("/admin/accounts")
  @ResponseBody
  public ModelAndView accounts(ModelAndView modelAndView) {
    modelAndView.setViewName("admin/accounts");
    return modelAndView;
  }

  @RequestMapping("/admin/accountModal")
  public ModelAndView accountModal(ModelAndView modelAndView) {
    modelAndView.setViewName("admin/accountModal");
    return modelAndView;
  }

  @RequestMapping("/403")
  public ModelAndView forbiddenPage(ModelAndView modelAndView) {
    modelAndView.setViewName("403");
    return modelAndView;
  }

  @RequestMapping("/404")
  public ModelAndView pageNotFound(ModelAndView modelAndView) {
    modelAndView.setViewName("404");
    return modelAndView;
  }

  @RequestMapping("/500")
  public ModelAndView internalServerError(ModelAndView modelAndView) {
    modelAndView.setViewName("500");
    return modelAndView;
  }

  @RequestMapping("/edit-account")
  public ModelAndView editAccount(ModelAndView modelAndView) {
    modelAndView.setViewName("editAccount");
    modelAndView.addObject("login", getLoggedInAccount().getLogin());
    return modelAndView;
  }

  @RequestMapping("/trainings-and-workshops")
  public ModelAndView eventsForUsersInCards(ModelAndView modelAndView) {
    modelAndView.setViewName("/user/eventsInCards");
    return modelAndView;
  }

  @RequestMapping("/hr/events/sign-up-requests")
  public ModelAndView signUpRequests(ModelAndView modelAndView) {
    modelAndView.setViewName("hr/signUpRequests");
    return modelAndView;
  }

  @RequestMapping("/events")
  public ModelAndView eventsAsUser(ModelAndView modelAndView) {
    modelAndView.setViewName("user/events");
    return modelAndView;
  }

  @RequestMapping("/event")
  public ModelAndView singleEvent(ModelAndView modelAndView, @RequestParam Long id, HttpServletRequest request) {
    modelAndView.setViewName("user/event");
    modelAndView.addObject("event", eventController.getSingleEvent(request, id));
    modelAndView.addObject("eventId", id);
    return modelAndView;
  }

  @RequestMapping("/navigation")
  public ModelAndView navigation(ModelAndView modelAndView) {
    try {
      AccountBean account = getLoggedInAccount();
      modelAndView.addObject("login", account.getLogin());
      modelAndView.addObject("account", account);
    } catch (NullPointerException e) {}
    modelAndView.setViewName("navigation");
    return modelAndView;
  }

  @RequestMapping("/footer")
  public ModelAndView footer(ModelAndView modelAndView) {
    modelAndView.setViewName("footer");
    return modelAndView;
  }

  private AccountBean getLoggedInAccount() {
    AccountBean bean = new AccountBean(accountService.getAccountByLogin(getCurrentLogin()));
    bean.setPassword(null);
    return bean;
  }
}
