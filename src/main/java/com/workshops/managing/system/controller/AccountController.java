package com.workshops.managing.system.controller;

import com.workshops.managing.system.model.bean.AccountBean;
import com.workshops.managing.system.model.bean.AccountTableBean;
import com.workshops.managing.system.model.entity.Account;
import com.workshops.managing.system.model.entity.Role;
import com.workshops.managing.system.model.exception.AppException;
import com.workshops.managing.system.service.AccountService;
import com.workshops.managing.system.utils.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static com.workshops.managing.system.controller.SecurityController.getCurrentLogin;

@RestController
@RequestMapping("account/")
@SessionAttributes("accountToEdit")
public class AccountController {

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(12);
  }

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Autowired
  AccountService accountService;

  @Autowired
  ApplicationEventPublisher eventPublisher;

  private void whitespacesCheck(String string) throws AppException {
    for (int i = 0; i < string.length(); ++i) {
      if (Character.isWhitespace(string.charAt(i))) {
        throw new AppException(new AppException.Error("Validation_Error1", "Whitespaces are not allowed."));
      }
    }
  }

  @RequestMapping(value = "register", produces = MediaType.APPLICATION_JSON_VALUE)
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
  public ResponseEntity<?> register(@RequestBody AccountBean accountBean) {
    try {
      if (accountBean.getLogin() == null || accountBean.getLogin().equals("")) {
        throw new AppException(new AppException.Error("Validation_Error2", "Login can not be empty!"));
      }
      if (accountBean.getLogin().length() < 4) {
        throw new AppException(new AppException.Error("Validation_Error3", "Login have to be longer than 3 characters!"));
      }
      if (accountService.loginExists(accountBean.getLogin())) {
        throw new AppException(new AppException.Error("Validation_Error4", "Login already exists."));
      }
      if (accountBean.getEmail() == null || accountBean.getEmail().equals("")) {
        throw new AppException(new AppException.Error("Validation_Error5", "Email can not be empty!"));
      }
      if (!accountBean.getEmail().contains("@")) {
        throw new AppException(new AppException.Error("Validation_Error6", "You have to type valid email address!"));
      }
      if (accountService.emailTaken(accountBean.getEmail())) {
        throw new AppException(new AppException.Error("Validation_Error7", "Account with such email exists!"));
      }
      if (accountBean.getPassword() == null || accountBean.getPassword().equals("")) {
        throw new AppException(new AppException.Error("Validation_Error8", "Password can not be empty!"));
      }
      if (accountBean.getPassword().length() < 8) {
        throw new AppException(new AppException.Error("Validation_Error9", "Password can not be shorter than 8 characters!"));
      }
      if (accountBean.getFirstName() == null || accountBean.getFirstName().equals("")) {
        throw new AppException(new AppException.Error("Validation_Error10", "First name can not be empty!"));
      }
      if (accountBean.getFirstName().length() < 2) {
        throw new AppException(new AppException.Error("Validation_Error11", "First name can not be shorter than 2 characters!"));
      }
      if (accountBean.getSurname() == null || accountBean.getSurname().equals("")) {
        throw new AppException(new AppException.Error("Validation_Error12", "Surname can not be empty!"));
      }
      if (accountBean.getSurname().length() < 2) {
        throw new AppException(new AppException.Error("Validation_Error13", "Surname can not be shorter than 2 characters!"));
      }
      whitespacesCheck(accountBean.getLogin());
      whitespacesCheck(accountBean.getEmail());
      whitespacesCheck(accountBean.getPassword());

      accountBean.setPassword(passwordEncoder.encode(accountBean.getPassword()));
      accountBean.setActive(false);
      accountBean.setEnabled(true);
      Account account = accountBean.create(accountBean);
      accountService.register(account);

      eventPublisher.publishEvent(new OnRegistrationCompleteEvent(account.getLogin()));

      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN')")
  @RequestMapping(value = "/getAccountsToTable", produces = MediaType.APPLICATION_JSON_VALUE)
  public AccountTableBean getAccountsToTable(
          @RequestParam("draw") Long draw,
          @RequestParam("order[0][column]") Long order,
          @RequestParam("order[0][dir]") String dir,
          @RequestParam("start") int start,
          @RequestParam("length") int length,
          @RequestParam("search[value]") String searchValue) {
    return accountService.getAccountsToTable(draw, order, dir, start, length, searchValue);
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "/getAccountToEdit", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getAccountToEdit(Model model, @RequestParam("login") String login) {
    try {
      Account account = accountService.getAccountByLogin(login);
      if (account == null) {
        throw new AppException(new AppException.Error("Account_not_found", "Account with such login does not exist!"));
      }
      model.asMap().clear();
      model.addAttribute("accountToEdit", account);
      AccountBean accountBean = new AccountBean(account);
      accountBean.setPassword(null);
      return new ResponseEntity<>(accountBean, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping(value = "/edit", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> editAccount(@ModelAttribute("accountToEdit") Account account, @RequestBody AccountBean accountBean) {
    try {
      if (account.getId() == null || account.getLogin() == null) {
        throw new AppException(new AppException.Error("Internal_server_error", ""));
      }
      if (!accountBean.getLogin().equals(account.getLogin())) {
        if (accountBean.getLogin() == null || accountBean.getLogin().equals("")) {
          throw new AppException(new AppException.Error("Validation_error2", "Login can not be empty!"));
        }
        if (accountBean.getLogin().length() < 4) {
          throw new AppException(new AppException.Error("Validation_error3", "Login have to be longer than 3 characters!"));
        }
        if (accountService.loginExists(accountBean.getLogin())) {
          throw new AppException(new AppException.Error("Validation_error4", "Login already exists."));
        }
      }
      if (!accountBean.getEmail().equals(account.getEmail())) {
        if (accountBean.getEmail() == null || accountBean.getEmail().equals("")) {
          throw new AppException(new AppException.Error("Validation_error5", "Email can not be empty!"));
        }
      }
      if (!accountBean.getEmail().contains("@")) {
        throw new AppException(new AppException.Error("Validation_error6", "You have to type valid email address!"));
      }
      if (accountBean.getPassword() != null && !accountBean.getPassword().equals("")) {
        if (accountBean.getPassword().length() < 8) {
          throw new AppException(new AppException.Error("Validation_error9", "Password can not be shorter than 8 characters!"));
        }
        if (passwordEncoder.matches(accountBean.getPassword(), account.getPassword())) {
          throw new AppException(new AppException.Error("Validation_error14", "You can not use existing password!"));
        }
        accountBean.setPassword(passwordEncoder.encode(accountBean.getPassword()));
      } else {
        accountBean.setPassword(account.getPassword());
      }
      if (accountBean.getFirstName() == null || accountBean.getFirstName().equals("")) {
        throw new AppException(new AppException.Error("Validation_error10", "First name can not be empty!"));
      }
      if (accountBean.getFirstName().length() < 2) {
        throw new AppException(new AppException.Error("Validation_error11", "First name can not be shorter than 2 characters!"));
      }
      if (accountBean.getSurname() == null || accountBean.getSurname().equals("")) {
        throw new AppException(new AppException.Error("Validation_error12", "Surname can not be empty!"));
      }
      if (accountBean.getSurname().length() < 2) {
        throw new AppException(new AppException.Error("Validation_error13", "Surname can not be shorter than 2 characters!"));
      }

      whitespacesCheck(accountBean.getLogin());
      whitespacesCheck(accountBean.getEmail());
      whitespacesCheck(accountBean.getPassword());

      Account accountToSave = accountBean.create(accountBean);
      accountToSave.setVersion(account.getVersion());
      accountToSave.setId(account.getId());

      accountService.editAccount(accountToSave, accountBean.getRole());

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      return new ResponseEntity<>(headers, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN')")
  @RequestMapping(value = "/getRoles", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getRoles() {
    try {
      Iterable<Role> roles = accountService.getRoles();
      if (roles == null || !roles.iterator().hasNext()) {
        throw new AppException(new AppException.Error("DB_Error", "There are no roles in database!\nPlease try again or contact support."));
      }
      List<String> rolesList = new ArrayList<>();
      roles.forEach(role -> rolesList.add(role.getName()));
      return new ResponseEntity<>(rolesList, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN')")
  @RequestMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createAccount(@RequestParam String login, @RequestBody AccountBean accountBean) {
    try {
      if (accountBean.getLogin() == null || accountBean.getLogin().equals("")) {
        throw new AppException(new AppException.Error("Validation_error2", "Login can not be empty!"));
      }
      if (accountBean.getLogin().length() < 4) {
        throw new AppException(new AppException.Error("Validation_error3", "Login have to be longer than 3 characters!"));
      }
      if (accountService.loginExists(accountBean.getLogin())) {
        throw new AppException(new AppException.Error("Validation_error4", "Login already exists."));
      }
      if (accountBean.getEmail() == null || accountBean.getEmail().equals("")) {
        throw new AppException(new AppException.Error("Validation_error5", "Email can not be empty!"));
      }
      if (!accountBean.getEmail().contains("@")) {
        throw new AppException(new AppException.Error("Validation_error6", "You have to type valid email address!"));
      }
      if (accountService.emailTaken(accountBean.getEmail())) {
        throw new AppException(new AppException.Error("Validation_error7", "Account with such email exists!"));
      }
      if (accountBean.getPassword() == null || accountBean.getPassword().equals("")) {
        throw new AppException(new AppException.Error("Validation_error8", "Password can not be empty!"));
      }
      if (accountBean.getPassword().length() < 8) {
        throw new AppException(new AppException.Error("Validation_error9", "Password can not be shorter than 8 characters!"));
      }
      if (accountBean.getFirstName() == null || accountBean.getFirstName().equals("")) {
        throw new AppException(new AppException.Error("Validation_error10", "First name can not be empty!"));
      }
      if (accountBean.getFirstName().length() < 2) {
        throw new AppException(new AppException.Error("Validation_error11", "First name can not be shorter than 2 characters!"));
      }
      if (accountBean.getSurname() == null || accountBean.getSurname().equals("")) {
        throw new AppException(new AppException.Error("Validation_error12", "Surname can not be empty!"));
      }
      if (accountBean.getSurname().length() < 2) {
        throw new AppException(new AppException.Error("Validation_error13", "Surname can not be shorter than 2 characters!"));
      }

      whitespacesCheck(accountBean.getLogin());
      whitespacesCheck(accountBean.getEmail());
      whitespacesCheck(accountBean.getPassword());

      accountBean.setPassword(passwordEncoder.encode(accountBean.getPassword()));
      accountBean.setEnabled(true);
      accountBean.setActive(true);
      Account account = accountBean.create(accountBean);
      accountService.createAccount(account);

      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN')")
  @RequestMapping(value = "/enable", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> enableAccount(@RequestParam String login) {
    try {
      accountService.enableAccount(login);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN')")
  @RequestMapping(value = "/disable", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> disableAccount(@RequestParam String login) {
    try {
      accountService.disableAccount(login);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @RequestMapping("reset-password")
  public ResponseEntity<?> resetPassword(@RequestParam String login, @RequestParam String email) {
    try {
      accountService.resetPassword(login, email);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  @PreAuthorize("hasAnyRole('ADMIN', 'HR', 'USER')")
  @RequestMapping("dismiss-notification")
  public ResponseEntity<?> dismissNotification(@RequestParam Long notificationId) {
    try {
      accountService.dismissNotification(getCurrentLogin(), notificationId);
      return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    } catch (AppException e) {
      return new ResponseEntity<>(e, HttpStatus.CONFLICT);
    } catch (Throwable e) {
      AppException appex = AppException.of(e);
      return new ResponseEntity<>(appex, HttpStatus.CONFLICT);
    }
  }

  private HttpHeaders httpHeaders;

  @PostConstruct
  private HttpHeaders initializeHttpHeaders() {
    httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    return httpHeaders;
  }
}
