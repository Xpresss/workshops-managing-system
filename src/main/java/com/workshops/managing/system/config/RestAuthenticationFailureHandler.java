package com.workshops.managing.system.config;

import com.workshops.managing.system.utils.SecurityUtils;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthenticationFailureHandler
extends SimpleUrlAuthenticationFailureHandler {
 
 @Override
 public void onAuthenticationFailure(HttpServletRequest request,
                                     HttpServletResponse response, AuthenticationException exception)
 throws IOException, ServletException {
  if (exception instanceof LockedException) {
   SecurityUtils.sendError(response, exception, HttpServletResponse.SC_UNAUTHORIZED,
       "Account_not_activated");
  }
  if (exception instanceof DisabledException) {
    SecurityUtils.sendError(response, exception, HttpServletResponse.SC_UNAUTHORIZED,
        "Account_banned");
  }
  SecurityUtils.sendError(response, exception, HttpServletResponse.SC_UNAUTHORIZED,
   "Auth_fail");
 }
}