package com.workshops.managing.system.config;

import com.workshops.managing.system.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;

  @Autowired
  private RestAuthenticationFailureHandler restAuthenticationFailureHandler;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .authorizeRequests()
          .antMatchers("/css/**", "/js/**", "/icons-images/**", "/dependencies/**").permitAll()
          .antMatchers("/", "/login", "/register", "/forgot-password", "/invalidToken**", "/tokenExpired**").permitAll()
          .antMatchers("/account/register", "/registrationConfirm*", "/resendRegistrationToken*", "/account/reset-password*").permitAll()
          .antMatchers("/trainings-and-workshops").permitAll()
          .antMatchers("/navigation", "/footer").permitAll()
          .antMatchers("/admin/**").hasRole("ADMIN")
          .antMatchers("/hr/**").hasRole("HR")
          .anyRequest().authenticated()
          .and()
        .formLogin()
          .loginProcessingUrl("/perform-login")
          .loginPage("/login").permitAll()
          .successHandler(restAuthenticationSuccessHandler)
          .failureHandler(restAuthenticationFailureHandler)
          .and()
        .logout()
          .logoutUrl("/logout").permitAll()
          .logoutSuccessUrl("/")
          .and().csrf().disable();
  }

  @Autowired
  private MyUserDetailsService userDetailsService;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService);
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth)
      throws Exception {
    auth.authenticationProvider(authenticationProvider());
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authProvider
        = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService);
    authProvider.setPasswordEncoder(encoder());
    return authProvider;
  }

  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder(12);
  }
}

