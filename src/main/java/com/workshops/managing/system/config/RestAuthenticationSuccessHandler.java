package com.workshops.managing.system.config;

import com.workshops.managing.system.service.AccountService;
import com.workshops.managing.system.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
 
 @Autowired
 private AccountService accountService;

  /**
   * Allows to return accountBean as response from 'perform-login' POST request in case of successful login.
   * When used redirecting after successful login doesn't work.
   * @param request
   * @param response
   * @param authentication
   * @throws ServletException
   * @throws IOException
   */
 @Override
 public void onAuthenticationSuccess(HttpServletRequest request,
                                     HttpServletResponse response, Authentication authentication)
 throws ServletException, IOException {
   SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
   if (savedRequest != null) {
     SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, savedRequest.getRedirectUrl());
   } else  {
     SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, "/");
   }
 }
}