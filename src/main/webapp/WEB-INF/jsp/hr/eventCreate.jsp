<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Create Event</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="../../../dependencies/css/nouislider.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../../../css/style.css">
  <link rel="stylesheet" href="../../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="headEventController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<header>
  <div ng-include="'/navigation'"></div>

  <nav class="white teal-text">
    <div class="nav-wrapper container truncate">
      <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
      <a href="/hr/events" class="breadcrumb teal-text">{{"events.management" | translate}}</a>
      <a href="#!" class="breadcrumb teal-text">{{"events.create" | translate}}</a>
    </div>
  </nav>
</header>

<main>
  <div class="container" style="margin-top: 30px">
    <div class="row" ng-controller="addEventController">
      <form id="eventForm" class="col s12" name="eventForm" novalidate>
        <div class="row bottom5">
          <div class="input-field col s6">
            <input placeholder="{{'events.eventName' | translate}}" id="title" name="title" type="text" class="validate" minlength="5"
                   required aria-required="true" ng-model="event.title">
            <label for="title">{{"events.title" | translate}}</label>
          </div>
          <div class="col s6">
            <label for="event-type">{{"events.type" | translate}}</label>
            <select id="event-type" name="eventType" class="icons" required aria-required="true">
              <option disabled selected>{{"events.chooseType" | translate}}</option>
              <option data-icon="./../../icons-images/sm-workshop-icon.png" class="circle" value="WORKSHOP">{{"events.WORKSHOP" | translate}}</option>
              <option data-icon="./../../icons-images/sm-training-icon.png" class="circle" value="TRAINING">{{"events.TRAINING" | translate}}</option>
            </select>
          </div>
        </div>
        <div class="row bottom5">
          <div class="input-field col s6">
            <input placeholder="{{'events.nameAndSurname' | translate}}" id="teacher" name="teacher" type="text" class="validate" required
                   aria-required="true" ng-model="event.teacher">
            <label for="teacher">{{"events.teacher" | translate}}</label>
          </div>
          <div class="input-field col s6">
            <input placeholder="{{'events.address' | translate}}" id="place" name="address" type="text" class="validate" required
                   aria-required="true" ng-model="event.place">
            <label for="place">{{"events.place" | translate}}</label>
          </div>
        </div>
        <div class="row bottom5">
          <div class="input-field col s12">
            <input id="capacity-input" name="capacity-input" type="text" class="validate col s2"
                   required aria-required="true" ng-model="event.capacity">
            <label style="margin-bottom: 100px">{{"events.personLimit" | translate}}</label>
            <div id="capacity-slider" class="col s8 offset-s1" style="margin-top: 40px;"></div>
          </div>
        </div>
        <div class="row bottom5">
          <div class="input-field col s4">
            <input type="text" class="datepicker" id="date" name="date_input" class="validate" required
                   aria-required="true" placeholder="dd MMM, yyyy">
            <label for="date">{{"events.date" | translate}}</label>
          </div>
          <div class="input-field col s4">
            <input type="text" class="timepicker" id="startTime" name="startTime" class="validate" required
                   aria-required="true" placeholder="hh:mm"
                   ng-model="event.startTime">
            <label for="date">{{"events.startTime" | translate}}</label>
          </div>
          <div class="input-field col s4">
            <input type="text" class="timepicker" id="endTime" name="endTime" class="validate" required
                   aria-required="true" placeholder="hh:mm"
                   ng-model="event.endTime">
            <label for="date">{{"events.endTime" | translate}}</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
          <textarea id="description" name="description" placeholder="{{'events.desc' | translate}}"
                    class="materialize-textarea validate" required aria-required="true"
                    ng-model="event.description" minlength="10"></textarea>
            <label for="description">{{"events.description" | translate}}</label>
          </div>
        </div>
        <div class="row">
          <div class="col s12 right-align">
            <button id="submitBtn" class="btn waves-effect waves-light" type="submit" name="action"
                    onclick="console.log($('#event-type').val());">{{"common.submit" | translate}}
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="../../../dependencies/jquery-3.2.1.min.js"></script>
<script src="../../../dependencies/nouislider.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="../../../js/EndpointService.js"></script>
<script src="../../../js/eventsControllers.js"></script>
<script src="../../../dependencies/jquery.validate.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../../dependencies/pickadate_pl_PL.js"></script>
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script src="../../../js/dateTimePickerInit.js"></script>
<script>
  $(document).ready(function () {
    $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
        $(element)
          .closest("form")
          .find("label[for='" + element.attr("id") + "']")
          .attr('data-error', error.text());
      },
      submitHandler: function (form) {
        console.log('form ok');
        angular.element($('#submitBtn')).scope().submitBtnOnClick();
      }
    });

    $.validator.addMethod("eventTypeRequired", function (value, element, arg) {
      console.log(value);
      console.log(element);
      console.log(arg);
      return arg !== value;
    }, "Value must not equal arg.");

    $("#eventForm").validate({
      rules: {
        eventType: {
          required: true,
          eventTypeRequired: null,
        },
        'capacity-input': {
          required: true
        }
      }
      ,
      messages: {
        eventType: {
          eventTypeRequired: "Please select event type!"
        }
      }
    });
  });
</script>
</body>
</html>
