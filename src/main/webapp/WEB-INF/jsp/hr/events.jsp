<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Events List</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="../../../dependencies/css/nouislider.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <%--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">--%>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../../../css/style.css">
  <link rel="stylesheet" href="../../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="headEventController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<header>
  <div ng-include="'/navigation'"></div>

  <nav class="white">
    <div class="nav-wrapper container truncate">
      <div class="row">
        <div class="col s12">
          <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
          <a href="events" class="breadcrumb teal-text">{{"events.management" | translate}}</a>
          <a href="#!" class="breadcrumb teal-text">{{"common.list" | translate}}</a>
        </div>
      </div>
    </div>
  </nav>
</header>

<main>
  <div class="container" style="margin-top: 35px" ng-controller="eventsGridController">

    <div class="row">
      <div class="col s12 m8 push-m4 l8 push-l4 hide-on-small-only">
        <a class="btn waves-effect waves-light green accent-4 right" type="button" name="addEventBtn"
           href="/hr/create-event">{{"events.new" | translate}}
          <i class="material-icons left">add</i>
        </a>
        <a class="btn waves-effect waves-light right" type="button" name="signUpRequestsBtn"
           href="/hr/events/sign-up-requests" style="margin-right: 10px;">
          {{"events.signUpRequests" | translate}}
        </a>
      </div>

      <div class="col s7 m4 pull-m8 l4 pull-l8" name="eventTypeSelector">
        <div name="eventTypeBtn"
             class="btn-floating light-blue darken-2 event-type-icon center-align tooltipped which-event-type-show-btn"
             data-position="bottom" data-tooltip="{{'events.tooltips.all' | translate}}" ng-click="getEventsByType($event, 'ALL')">{{"events.list.all" | translate}}
        </div>
        <div name="eventTypeBtn"
             class="btn-floating yellow event-type-icon center-align tooltipped which-event-type-show-btn"
             data-position="bottom" data-tooltip="{{'events.tooltips.onlyWorkshops' | translate}}"
             ng-click="getEventsByType($event, 'WORKSHOPS')">{{"events.list.workshops" | translate}}
        </div>
        <div name="eventTypeBtn"
             class="btn-floating red event-type-icon center-align tooltipped which-event-type-show-btn"
             data-position="bottom" data-tooltip="{{'events.tooltips.onlyTrainings' | translate}}"
             ng-click="getEventsByType($event, 'TRAININGS')">{{"events.list.trainings" | translate}}
        </div>
      </div>
    </div>

    <div class="row">
      <div style="overflow-x:auto;">
        <table id="eventsTable" class="mdl-data-table highlight" cellspacing="0" style="width: 100%">
          <thead>
          <tr>
            <th>id</th>
            <th>Type</th>
            <th>Title</th>
            <th>Description</th>
            <th>Teacher</th>
            <th>Place</th>
            <th>Date Time</th>
            <th></th>
          </tr>
          </thead>
        </table>
      </div>
    </div>

    <div id="editEventModal" class="modal modal-fixed-footer">
      <nav class="valign-wrapper">
        <h4 style="margin: auto">{{"events.eventEdition" | translate}}</h4>
      </nav>
      <form id="eventForm" name="eventForm" novalidate="novalidate">
        <div class="modal-content">
          <div class="row">
            <div class="input-field col s6">
              <input placeholder="Event name" id="title" name="title" type="text" class="validate"
                     required aria-required="true" ng-model="event.title">
              <label for="title">{{"events.title" | translate}}</label>
            </div>
            <div class="col s6">
              <label for="event-type">{{"events.type" | translate}}</label>
              <select id="event-type" class="icons">
                <option disabled selected>{{"events.chooseType" | translate}}</option>
                <option data-icon="./../../icons-images/sm-workshop-icon.png" class="circle" value="WORKSHOP">{{"events.WORKSHOP" | translate}}</option>
                <option data-icon="./../../icons-images/sm-training-icon.png" class="circle" value="TRAINING">{{"events.TRAINING" | translate}}</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <input placeholder="Name and Surname" id="teacher" name="teacher" type="text" class="validate" required
                     aria-required="true" ng-model="event.teacher">
              <label for="teacher">{{"events.teacher" | translate}}</label>
            </div>
            <div class="input-field col s6">
              <input placeholder="Address" id="place" name="address" type="text" class="validate" required
                     aria-required="true" ng-model="event.place">
              <label for="place">{{"events.place" | translate}}</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input id="capacity-input" name="capacity-input" type="text" class="validate col s2"
                     required aria-required="true" ng-model="event.capacity">
              <label style="margin-bottom: 100px">{{"events.personLimit" | translate}}</label>
              <div id="capacity-slider" class="col s8 offset-s1" style="margin-top: 40px;"></div>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s4">
              <input type="text" class="datepicker" id="date" name="date_input" class="validate" required
                     aria-required="true" placeholder="dd MMM, yyyy">
              <label for="date">{{"events.date" | translate}}</label>
            </div>
            <div class="input-field col s4">
              <input type="text" class="timepicker" id="startTime" name="startTime" class="validate" required
                     aria-required="true" placeholder="hh:mm"
                     ng-model="event.startTime">
              <label for="date">{{"events.startTime" | translate}}</label>
            </div>
            <div class="input-field col s4">
              <input type="text" class="timepicker" id="endTime" name="endTime" class="validate" required
                     aria-required="true" placeholder="hh:mm"
                     ng-model="event.endTime">
              <label for="date">{{"events.endTime" | translate}}</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
                <textarea id="description" name="description" placeholder="Few sentences about this event"
                          class="materialize-textarea validate" required aria-required="true"
                          ng-model="event.description"></textarea>
              <label for="description">{{"events.description" | translate}}</label>
            </div>
          </div>
        </div>
        <div class="modal-footer" ng-controller="editEventController">
          <div class="row">
            <div class="col s12 right-align">
              <button class="btn waves-effect waves-light red modal-close" type="submit" name="action">{{"common.close" | translate}}
                <i class="material-icons right">close</i>
              </button>
              <button class="btn waves-effect waves-light" type="submit" name="action" ng-click="submitBtnOnClick()">
                {{"common.submit" | translate}}
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>

    <div id="participants" class="modal modal-fixed-footer">
      <nav class="valign-wrapper">
        <h4 style="margin: auto">{{eventTitleForParticipantsModal}} - {{"events.participants" | translate}}</h4>
      </nav>
      <div class="modal-content">
        <table>
          <thead>
          <tr>
            <th></th>
            <th>{{"loginRegisterPage.username" | translate}}</th>
            <th>{{"loginRegisterPage.firstName" | translate}}</th>
            <th>{{"loginRegisterPage.surname" | translate}}</th>
          </tr>
          </thead>
          <tbody>
          <tr ng-repeat="participant in participants">
            <td>{{$index + 1}}</td>
            <td>{{participant.login}}</td>
            <td>{{participant.firstName}}</td>
            <td>{{participant.surname}}</td>
            <td><a ng-click="removeParticipant(participant.login)"
                   class="btn-floating waves-effect waves-light red tooltipped" data-position="right"
                   data-tooltip="Remove participant"><i class="large material-icons">delete</i></a></td>
          </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
      </div>
    </div>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="../../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.4.0/b-colvis-1.4.0/b-flash-1.4.0/b-html5-1.4.0/b-print-1.4.0/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.3.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="../../../dependencies/nouislider.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="../../../js/EndpointService.js"></script>
<script src="../../../js/eventsControllers.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="./../../dependencies/moment.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../../dependencies/pickadate_pl_PL.js"></script>
</c:if>
<script src="../../../js/dateTimePickerInit.js"></script>
</body>
</html>
