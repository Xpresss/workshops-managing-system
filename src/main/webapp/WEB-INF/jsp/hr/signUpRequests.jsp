<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
  <title>Sign up requests management</title>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link rel="stylesheet" href="../../../css/style.css">
  <link rel="stylesheet" href="../../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="signUpRequestsController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<header>
  <div ng-include="'/navigation'"></div>

  <nav class="white">
    <div class="nav-wrapper container">
      <div class="row">
      <div class="col s12 truncate">
        <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
        <a href="/hr/events" class="breadcrumb teal-text">{{"events.management" | translate}}</a>
        <a href="#!" class="breadcrumb teal-text">{{"events.signUpRequests" | translate}}</a>
      </div>
      </div>
    </div>
  </nav>
</header>

<main>
  <div class="container">
    <table class="highlight responsive-table" ng-init="init()">
      <thead>
      <tr class="hide-on-med-and-down">
        <th class="hide-on-med-and-down" colspan="3" style="border-bottom: solid 1px #d0d0d0; font-size: larger">Event
        </th>
        <th class="hide-on-med-and-down" colspan="3" style="border-bottom: solid 1px #d0d0d0; font-size: larger">User
        </th>
      </tr>
      <tr>
        <th id="SU-name">{{"events.title" | translate}}</th>
        <th id="SU-date">{{"events.date" | translate}}</th>
        <th id="SU-seats" class="tooltipped" data-tooltip="Occupied / Limit">{{"events.seats" | translate}}</th>
        <th>{{"loginRegisterPage.username" | translate}}</th>
        <th>{{"loginRegisterPage.firstName" | translate}}</th>
        <th>{{"loginRegisterPage.surname" | translate}}</th>
        <th id="SU-actions"></th>
      </tr>
      </thead>
      <tbody>
      <tr ng-repeat="request in requests">
        <td>{{request.eventName}}</td>
        <td>{{request.eventDate}}</td>
        <td>{{request.noAccepted}} / {{request.capacity}}</td>
        <td>{{request.userLogin}}</td>
        <td>{{request.userName}}</td>
        <td>{{request.userSurname}}</td>
        <td>
          <div class="btn btn-floating white tooltipped" data-position="left" data-tooltip="Accept request"
               ng-click="considerSignUpRequest(request.eventId, request.userLogin, true)">
            <i class="material-icons" style="color: green">check</i>
          </div>
          <div class="btn btn-floating white tooltipped" data-position="right" data-tooltip="Reject request"
               ng-click="considerSignUpRequest(request.eventId, request.userLogin, false)">
            <i class="material-icons" style="color: darkred">close</i>
          </div>
        </td>
      </tr>
      </tbody>
    </table>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="../../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="../../../js/EndpointService.js"></script>
<script src="../../../js/eventsControllers.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
</body>
</html>
