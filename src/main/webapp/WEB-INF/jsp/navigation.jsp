<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<input id="loginFromModel" ng-model="loginFromModel" value="${login}" type="hidden">

<nav ondragstart="return false;" class="cyan">
  <div class="nav-wrapper container">
    <a href="/" class="brand-logo center">Logo</a>
    <a href="#" data-activates="mobile-nav" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul class="left hide-on-med-and-down" id="navi">
      <li ng-if="${account.role == 'ADMIN'}">
        <a href="#" class="dropdown-button waves-effect waves-cyan" data-activates="admin-dropdown"><i class="material-icons right">arrow_drop_down</i>{{ 'navigation.admin' | translate }}</a>
        <ul id="admin-dropdown" class="dropdown-content">
          <li><a href="/admin/accounts" class="waves-effect waves-cyan"><i class="material-icons left">people</i>{{ 'navigation.accounts' | translate }}</a></li>
        </ul>
      </li>
      <li ng-if="${account.role == 'HR'}">
        <a href="#" class="dropdown-button waves-effect waves-cyan" data-activates="hr-dropdown"><i class="material-icons right">arrow_drop_down</i>HR</a>
        <ul id="hr-dropdown" class="dropdown-content">
          <li><a href="/hr/events" class="waves-effect waves-cyan"><i class="material-icons left">event_note</i>{{ 'navigation.events' | translate }}</a></li>
          <li><a href="/hr/events/sign-up-requests" class="waves-effect waves-cyan"><i class="material-icons left">event_available</i>{{ 'navigation.signUpRequests' | translate }}</a></li>
          <li><a href="/hr/create-event" class="waves-effect waves-cyan"><i class="material-icons left">add_to_photos</i>{{ 'navigation.createEvent' | translate }}</a></li>
        </ul>
      </li>
      <li ng-if="${account.role == 'ADMIN' || account.role == 'HR' || account.role == 'USER'}">
        <a href="#" class="dropdown-button waves-effect waves-cyan" data-activates="user-dropdown"><i class="material-icons right">arrow_drop_down</i>{{ 'navigation.USER' | translate }}</a>
        <ul id="user-dropdown" class="dropdown-content">
          <li><a href="/events" class="waves-effect waves-cyan"><i class="material-icons left">event_note</i>{{ 'navigation.events' | translate }}</a></li>
        </ul>
      </li>
    </ul>

    <ul class="right" ng-if="${not empty login}">
      <li>
        <a class="dropdown-button waves-effect waves-cyan" data-activates="notifications-dropdown"><i
            class="material-icons">notifications</i></a>
        <ul id="notifications-dropdown" class="dropdown-content" ng-controller="notificationsController">
          <li><span class="no-swipe"><b class="no-swipe">{{ 'navigation.notifications' | translate }}</b>
              <c:if test="${fn:length(account.notifications) != 0}">
              <span id="notification-counter"
                    class="new badge no-swipe">${fn:length(account.notifications)}</span>
              </c:if>
            </span></li>
          <li class="divider"></li>
          <c:forEach items="${account.notifications}" var="notification">
            <li ng-if="${notification.type == "EVENT_ONCOMING"}" notification-id="${notification.id}">
              <a href="/event?id=${notification.eventId}" class="waves-effect waves-cyan"><i class="material-icons left">today</i>{{ 'navigation.oncoming' | translate }}</a>
            </li>
            <li ng-if="${notification.type == "SIGNUP_ACCEPTED"}" notification-id="${notification.id}">
              <a href="/event?id=${notification.eventId}" class="waves-effect waves-cyan"><i class="material-icons left">check</i>{{ 'navigation.signUpAccepted' | translate }}</a>
            </li>
            <li ng-if="${notification.type == "SIGNUP_REJECTED"}" notification-id="${notification.id}">
              <a href="/event?id=${notification.eventId}" class="waves-effect waves-cyan"><i class="material-icons left">clear</i>{{ 'navigation.signUpRejected' | translate }}</a>
            </li>
            <li ng-if="${notification.type == "EVENT_EDITED"}" notification-id="${notification.id}">
              <a href="/event?id=${notification.eventId}" class="waves-effect waves-cyan"><i class="material-icons left">edit</i>{{ 'navigation.eventEdited' | translate }}</a>
            </li>
            <li ng-if="${notification.type == "NEW_SIGNUP_REQUEST"}" notification-id="${notification.id}">
              <a href="/hr/events/sign-up-requests" class="waves-effect waves-cyan"><i class="material-icons left">person_add</i>{{ 'navigation.newSignUp' | translate }}</a>
            </li>
          </c:forEach>
        </ul>
      </li>
      <li class="hide-on-med-and-down">
        <a href="#" class="dropdown-button waves-effect waves-cyan" data-activates="account-dropdown">${login}<i class="material-icons left">person</i></a>
        <ul id="account-dropdown" class="dropdown-content">
          <li><a href="/edit-account" class="waves-effect waves-cyan"><i class="material-icons left">edit</i>{{ 'navigation.editAccount' | translate }}</a></li>
        </ul>
      </li>
      <li class="hide-on-med-and-down"><a href="/logout" class="waves-effect waves-cyan"><i class="fa fa-sign-out left" aria-hidden="true"></i>{{ 'navigation.logout' | translate }}</a></li>
    </ul>
    <ul class="right hide-on-med-and-down" ng-if="${empty login}">
      <li><a href="/register" class="waves-effect waves-cyan"><i class="fa fa-user-plus left" aria-hidden="true"></i>{{ 'navigation.register' | translate }}</a></li>
      <li><a href="/login" class="waves-effect waves-cyan"><i class="fa fa-sign-in left" aria-hidden="true"></i>{{ 'navigation.login' | translate }}</a></li>
    </ul>
    <ul class="side-nav" id="mobile-nav">
      <ul class="collapsible" data-collapsible="accordion">
        <li ng-if="${account.role == 'ADMIN'}">
          <a class="collapsible-header waves-effect waves-cyan"><i class="material-icons right">arrow_drop_down</i>{{ 'navigation.admin' | translate }}</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="/admin/accounts" class="waves-effect waves-cyan"><i class="material-icons left">people</i>{{ 'navigation.accounts' | translate }}</a></li>
            </ul>
          </div>
        </li>
        <li ng-if="${account.role == 'ADMIN' || account.role == 'HR'}">
          <a class="collapsible-header waves-effect waves-cyan"><i class="material-icons right">arrow_drop_down</i>HR</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="/hr/events" class="waves-effect waves-cyan"><i class="material-icons left">event_note</i>{{ 'navigation.events' | translate }}</a></li>
              <li><a href="/hr/events/sign-up-requests" class="waves-effect waves-cyan"><i class="material-icons left">event_available</i>{{ 'navigation.signUpRequests' | translate }}</a></li>
              <li><a href="/hr/create-event" class="waves-effect waves-cyan"><i class="material-icons left">add_to_photos</i>{{ 'navigation.createEvent' | translate }}</a></li>
            </ul>
          </div>
        </li>
        <li ng-if="${account.role == 'ADMIN' || account.role == 'HR' || account.role == 'USER'}">
          <a class="collapsible-header waves-effect waves-cyan"><i class="material-icons right">arrow_drop_down</i>{{ 'navigation.USER' | translate }}</a>
          <div class="collapsible-body">
            <ul>
              <li><a href="/events" class="waves-effect waves-cyan"><i class="material-icons left">event_note</i>{{ 'navigation.events' | translate }}</a></li>
            </ul>
          </div>
        </li>
      </ul>
      <li><div class="divider"></div></li>
      <li ng-if="${not empty login}"><a href="/edit-account" class="waves-effect waves-cyan"><i class="material-icons left">edit</i>{{ 'navigation.editAccount' | translate }}</a></li>
      <li ng-if="${not empty login}"><a href="/logout" class="waves-effect waves-cyan"><i class="fa fa-sign-out left" aria-hidden="true"></i>{{ 'navigation.logout' | translate }}</a></li>
      <li ng-if="${empty login}"><a href="/register" class="waves-effect waves-cyan"><i class="fa fa-user-plus left" aria-hidden="true"></i>{{ 'navigation.register' | translate }}</a></li>
      <li ng-if="${empty login}"><a href="/login" class="waves-effect waves-cyan"><i class="fa fa-sign-in left" aria-hidden="true"></i>{{ 'navigation.login' | translate }}</a></li>
    </ul>
    </div>
  </div>
</nav>

<style>
  #navi .dropdown-content > li > a > i {
    margin: 0 5px 0 0;
  }

  #account-dropdown a > i {
    margin: 0 5px 0 0;
  }
</style>

<script>
  $(document).ready(function () {
    $('.dropdown-button').dropdown({
        constrainWidth: false,
        belowOrigin: true,
      }
    );
    $(".button-collapse").sideNav();
    $('.collapsible').collapsible();
  });
</script>