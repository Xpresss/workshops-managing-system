<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
  <title>Title</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.1.19/css/materialdesignicons.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body ng-app="notificationsApp" ondragstart="return false;">

<div id="loader-wrapper">
  <div id="loader"></div>

  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>

</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<div ng-include="'navigation'"></div>

<div class="parallax-container">
  <div class="parallax"><img src="./../../icons-images/event1.jpg"></div>
</div>
<div class="section white">
  <div class="row container page-description">
    <h2 class="header">{{'home.eventsManagingSystem' | translate }}</h2>
    <p class="grey-text text-darken-3 lighten-3">{{'home.description' | translate }}</p>
  </div>
</div>
<div class="parallax-container">
  <div class="parallax"><img src="./../../icons-images/event2.jpg"></div>
</div>

<div ng-include="'/footer'"></div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="./../../js/EndpointService.js"></script>
<script src="./../../js/common.js"></script>
<script src="../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>

</body>
</html>