<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <style>
    i {
      ms-transform: translate(0, 20px);
      webkit-transform: translate(0, 20px);
      transform: translate(0,20px);
    }
  </style>
  <title>500</title>
</head>
<body class="cyan" ng-app="at">
<div class="container">
  <div class="row">
    <br>
    <br>
    <br>
    <div class="col s12 white z-depth-4" style="border-radius: 3px">
      <h2 class="center"><i class="material-icons large">sentiment_dissatisfied</i>500 {{"500.title" | translate}}</h2>
      <h4 class="center">{{"500.desc" | translate}}</h4>
      <h5 class="center">{{"accActivation.contact" | translate}}: workshops-managing-system@gmail.com</h5>
    </div>
  </div>
</div>
<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
</body>
</html>