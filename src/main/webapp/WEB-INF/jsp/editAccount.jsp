<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Edit account</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="editAccountController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<header>
  <div ng-include="'/navigation'"></div>

  <nav class="white">
    <div class="nav-wrapper container">
      <div class="row">
        <div class="col s12">
          <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
          <a href="#!" class="breadcrumb teal-text">{{'common.accountEdition' | translate}}</a>
        </div>
      </div>
    </div>
  </nav>
</header>

<main>
  <input id="loginFromModel" ng-model="loginFromModel" value="${login}" type="hidden">

  <div class="container" style="margin-top: 50px">
    <div class="row">
      <form id="accountForm" name="accountForm" novalidate="novalidate">
        <div class="modal-content valign-wrapper">
          <div class="row">
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">person_outline</i>
              <input id="login" type="text" name="login" ng-model="account.login" required aria-required="true"
                     minlength="4" disabled class="tooltipped" data-position="bottom" data-delay="50"
                     data-tooltip="Only ADMIN can change login.">
              <label for="login">{{'loginRegisterPage.username' | translate}}</label>
            </div>
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">mail_outline</i>
              <input id="mail" type="email" name="email" ng-model="account.email" required aria-required="true">
              <label for="mail">Email</label>
            </div>
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">border_color</i>
              <input id="firstName" type="text" name="firstName" ng-model="account.firstName" required
                     aria-required="true" minlength="2">
              <label for="firstName">{{'loginRegisterPage.firstName' | translate}}</label>
            </div>
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">gesture</i>
              <input id="surname" type="text" name="surname" ng-model="account.surname" required
                     aria-required="true"
                     minlength="2">
              <label for="surname">{{'loginRegisterPage.surname' | translate}}</label>
            </div>
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">lock_outline</i>
              <input id="password" type="password" name="password" ng-model="account.password" minlength="8">
              <label for="password">{{'loginRegisterPage.password' | translate}}</label>
            </div>
            <div class="col s12 m12 l6 input-field input-margin">
              <i class="material-icons prefix">lock_outline</i>
              <input id="passwordConfirm" type="password" name="passwordConfirm"
                     ng-model="account.passwordConfirm"
                     minlength="8">
              <label for="passwordConfirm">{{'loginRegisterPage.confirmPassword' | translate}}</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row">
            <i class="col s12" style="color: grey">{{'common.emptyPasswd' | translate}}</i>
            <div class="col s12 right-align">
              <button id="submitBtn" class="btn waves-effect waves-light" type="submit" name="action">{{'common.submit'
                | translate}}
                <i class="material-icons right">send</i>
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>

<div ng-include="'/footer'" onload="onFooterLoad()"></div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="./../../js/EndpointService.js"></script>
<script src="./../../dependencies/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
<script src="./../../js/editAccountController.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<script>
  jQuery.extend(jQuery.validator.messages, {
    equalTo: "Passwords do not match.",
  });

  jQuery.validator.addMethod("noSpace", function (value, element) {
    return value.indexOf(" ") < 0;
  }, "Whitespaces are not allowed.");
</script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script>
  $(document).ready(function () {
    $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
        $(element)
          .closest("form")
          .find("label[for='" + element.attr("id") + "']")
          .attr('data-error', error.text());
      }
    });

    $("#accountForm").validate({
      rules: {
        password: {
          noSpace: true
        },
        passwordConfirm: {
          equalTo: "#password",
          noSpace: true
        },
        login: {
          noSpace: true
        },
        email: {
          noSpace: true
        }
      },
      submitHandler: function () {
        console.log('form ok');
        angular.element($('#submitBtn')).scope().submitBtnOnClick();
      }
    });
  });
</script>
</body>
</html>
