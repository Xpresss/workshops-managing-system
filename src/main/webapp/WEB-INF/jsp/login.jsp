<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>Login</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="../../css/login-register-style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" class="cyan">
<div id="loader-wrapper">
  <div id="loader"></div>

  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>

</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<input value="${activated}" id="activated" hidden>

<div class="row" ng-controller="loginController">
  <div class="col s 12 z-depth-4 card-panel">
    <form id="login-form" class="login-form" novalidate="novalidate">
      <div class="row">
        <div class="col s12 center input-field">
          <img src="./../../icons-images/training_icon4.png" class="yellow responsive-img logo">
          <p class="center login-form-title">{{ 'logo1' | translate }} <br> {{ 'logo2' | translate }}</p>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">person_outline</i>
          <input id="login" type="text" class="validate" name=login required="required" aria-required="true" ng-model="credentials.username">
          <label for="login">{{ 'loginRegisterPage.username' | translate }}</label>
        </div>
      </div>
      <div class="row margin" style="padding-bottom: 5px">
        <div class="col s12 input-field">
          <i class="material-icons prefix">lock_outline</i>
          <input id="password" type="password" class="validate" name="password" required="required" aria-required="true" ng-model="credentials.password">
          <label for="password">{{ 'loginRegisterPage.password' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 center input-field">
          <button id="loginBtn" class="btn col s12 waves-effect waves-light">
            {{ 'loginRegisterPage.login' | translate }}
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col s6 input-field">
          <p class="margin medium-small">
            <a href="register">{{ 'loginRegisterPage.needAccount' | translate }}</a>
          </p>
        </div>
        <div class="col s6 input-field">
          <p class="margin right-align medium-small">
            <a href="forgot-password">{{ 'loginRegisterPage.forgotPassword' | translate }}</a>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="./../../js/EndpointService.js"></script>
<script src="./../../dependencies/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
<script src="./../../js/UserIdentityService.js"></script>
<script src="./../../js/loginController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script>
  $(document).ready(function () {
    $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
        $(element)
          .closest("form")
          .find("label[for='" + element.attr("id") + "']")
          .attr('data-error', error.text());
      }
    });


    $("#login-form").validate({
      rules: {
        password: {
          required: true
        },
        login: {
          required: true
        }
      },
      submitHandler: function () {
        console.log('form ok');
        angular.element('#loginBtn').scope().performLogin();
      }
    });

    $(".button-collapse").sideNav();
  })
</script>
</body>
</html>
