<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Event</title>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="eventController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<input id="event" value="${event}" type="hidden">
<input id="eventId" value="${eventId}" type="hidden">

<header>
  <div ng-include="'navigation'"></div>

  <nav class="white teal-text">
    <div class="nav-wrapper container truncate">
      <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
      <a href="/events" class="breadcrumb teal-text">{{"events.events" | translate}}</a>
      <a href="#" class="breadcrumb teal-text">${event.title}</a>
    </div>
  </nav>
</header>

<main>
  <div class="container">
    <div>
      <a class="btn-large waves-effect waves-light right" ng-click="signUp(${event.id})"><i
          class="material-icons right">group_add</i>{{"events.signUp" | translate}}</a>
      <h3><a event-type="${event.eventType}"
             class="btn-floating tooltipped event-type-icon center-align standard-cursor event-type-icon2"
             style="margin-right: 15px; margin-top: -6px"></a>${event.title}</h3>
      <span>${event.description}</span>
      <p class="tooltipped event-detail" data-tooltip="Location" data-position="left"><i
          class="material-icons left small teal-text">place</i>${event.place}</p>
      <p class="tooltipped event-detail" data-tooltip="Lecturer" data-position="left"><i
          class="material-icons left small teal-text">person</i>${event.teacher}</p>
      <p class="tooltipped event-detail" data-tooltip="Date" data-position="left"><i
          class="material-icons left small teal-text">today</i>${event.startTime} - ${event.endTime}, ${event.date}</p>
      <p class="tooltipped event-detail" data-tooltip="Sign up's / Person limit" data-position="left"><i
          class="material-icons left small teal-text">group</i>${event.signUps  } / ${event.capacity}</p>
    </div>

    <div class="row" style="margin-top: 75px">
      <form id="commentForm" novalidate>
        <div class="input-field col s12">
        <textarea id="comment" class="materialize-textarea" required aria-required="true" ng-model="reviewToAdd"
                  minlength="20"></textarea>
          <label for="comment">{{"events.yourReview" | translate}}</label>
        </div>
        <button type="submit" class="btn right"><i class="material-icons right">send</i>{{"common.submit" | translate}}</button>
      </form>
    </div>

    <div class="row">
      <div class="col s12"><h5 class="teal-text">{{"events.reviews" | translate}}</h5></div>
    </div>

    <c:forEach items="${event.reviews}" var="review">

      <div class="row">
        <div class="col s12 review z-depth-3">
          <div class="col 6" style="margin-top: 5px">
            <span class="left">${review.user}</span>
          </div>
          <div class="col s6 right" style="margin-top: 5px">
            <span class="right">${review.date}</span>
          </div>
          <div class="col s12">
            <p>${review.comment}</p>
          </div>
        </div>
      </div>
    </c:forEach>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="../../../dependencies/jquery.validate.js"></script>
<script src="./../../../js/EndpointService.js"></script>
<script src="./../../js/eventController.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script>
  $(document).ready(function () {
    $.validator.setDefaults({
      errorClass: 'invalid',
      validClass: "valid",
      errorPlacement: function (error, element) {
        $(element)
          .closest("form")
          .find("label[for='" + element.attr("id") + "']")
          .attr('data-error', error.text());
      },
      submitHandler: function (form) {
        console.log('form ok');
        angular.element($('#commentForm')).scope().addReview();
      }
    });

    $("#commentForm").validate({});
  });
</script>
</body>
</html>
