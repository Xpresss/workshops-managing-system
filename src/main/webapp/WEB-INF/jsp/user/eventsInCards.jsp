<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<style>
  #card-reveal i {
    color: #3498db;
  }
</style>
<div class="card">
  <div class="card-image waves-effect waves-block waves-light">
    <img class="activator random-event-image" src="" alt="If you can see this, then the image didn't load properly">
    <a event-type="{{event.eventType}}" class="btn-floating tooltipped event-type-icon center-align standard-cursor event-type-on-card"></a>
  </div>
  <div class="card-content">
    <i class="material-icons right activator card-title">more_vert</i>
    <span class="card-title activator grey-text text-darken-4 to-resize">{{event.title}}</span>
    <a class="btn waves-effect waves-light" ng-if="event.canSignUp" ng-click="signUp(event.id)"><i class="material-icons right">group_add</i>{{"events.signUp" | translate}}</a>
  </div>
  <div class="card-reveal" id="card-reveal">
    <span class="card-title grey-text text-darken-4"><i class="material-icons right">close</i>{{event.title}}</span>
    <p>{{event.description}}</p>
    <p class="tooltipped" data-tooltip="Location" data-position="left"><i class="material-icons left">place</i>{{event.place}}</p>
    <p class="tooltipped" data-tooltip="Lecturer" data-position="left"><i class="material-icons left">person</i>{{event.teacher}}</p>
    <p class="tooltipped" data-tooltip="Date" data-position="left"><i class="material-icons left">today</i>{{event.startTime}} - {{event.endTime}}, {{event.date}}</p>
    <p class="tooltipped" data-tooltip="Sign up's / Person limit" data-position="left"><i class="material-icons left">group</i>{{event.signUps}} / {{event.capacity}}</p>
    <a class="btn waves-effect waves-light col s12" ng-click="goToReviews(event.id)" style="margin-bottom: 20px"><i class="material-icons right white-text">rate_review</i>{{"events.reviews" | translate}}</a>
  </div>
</div>
<script>
  var imgs = document.getElementsByClassName('random-event-image');

  for (var i = 0; i < imgs.length; i++) {
    var num = Math.floor(Math.random() * 8 + 1);
    imgs[i].src = './../../icons-images/event' + num + '.jpg';
    imgs[i].alt = imgs[i].src;
  }
</script>