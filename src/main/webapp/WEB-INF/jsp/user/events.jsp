<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
  <title>Events (Workshops and Trainings)</title>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="eventsController" ondragstart="return false;">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<header>
  <div ng-include="'navigation'"></div>

  <nav class="white teal-text">
    <div class="nav-wrapper container">
      <div class="row">
        <div class="col s12">
          <a href="/" class="breadcrumb white teal-text"><i class="material-icons">home</i></a>
          <a href="/events" class="breadcrumb white teal-text">{{"events.events" | translate}}</a>
        </div>
      </div>
    </div>
  </nav>
</header>

<main>
  <div class="container">

    <br/>
    <div class="row">
      <div class="col s12">
        <div id="filter-btn" class="btn-large red waves-effect waves-light"><i class="material-icons large right">filter_list</i>{{"events.filter" | translate}}
        </div>
        <ul id="filter-tabs" class="tabs">
          <li class="tab col s6 m3"><a href="" ng-click="showAllEvents()">{{"events.allEvents" | translate}}</a></li>
          <li class="tab col s6 m3"><a href="" ng-click="showOncomingEvents()">{{"events.oncomingEvents" | translate}}</a></li>
          <li class="tab col s6 m3"><a href="" ng-click="showMyEvents()">{{"events.myEvents" | translate}}</a></li>
          <li class="tab col s6 m3"><a href="" ng-click="showEventsToRate()">{{"events.toReview" | translate}}</a></li>
        </ul>
        <div id="search-btn" class="btn-large waves-effect waves-light blue right"><i
            class="material-icons large right">search</i>{{"events.search" | translate}}
        </div>
        <div id="search-input-field" class="input-field col s3 right">
          <form id="searchForm">
            <input id="search" type="text" ng-model="filter">
            <label for="search">{{"events.search" | translate}}</label>
          </form>
        </div>
      </div>
    </div>

    <div id="events-in-cards" class="row" ng-init="init()">
      <div class="col s12 m6 l4" ng-repeat="event in events">
        <div ng-include="'trainings-and-workshops'"></div>
      </div>
    </div>

    <h3 ng-show="noResults" class="center">{{"events.noResults" | translate}}</h3>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="./../../../dependencies/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="./../../../js/EndpointService.js"></script>
<script src="./../../../js/eventsAsUserController.js"></script>
<script src="./../../dependencies/slip.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
</body>
</html>
