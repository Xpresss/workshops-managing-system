<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div id="accountModal" class="modal modal-fixed-footer">
  <nav class="valign-wrapper">
    <h4 style="margin: auto">{{modalHeader}} <u ng-if="edit">{{login}}</u></h4>
  </nav>
  <form id="accountForm" name="accountForm" novalidate="novalidate">
    <div class="modal-content">
      <div class="row">
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">person_outline</i>
          <input id="login" type="text" name="login" ng-model="$parent.account.login" required aria-required="true"
                 minlength="4">
          <label for="login">{{"loginRegisterPage.username" | translate}}</label>
        </div>
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">mail_outline</i>
          <input id="mail" type="email" name="email" ng-model="$parent.account.email" required aria-required="true">
          <label for="mail">Email</label>
        </div>
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">border_color</i>
          <input id="firstName" type="text" name="firstName" ng-model="$parent.account.firstName" required
                 aria-required="true" minlength="2">
          <label for="firstName">{{"loginRegisterPage.firstName" | translate}}</label>
        </div>
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">gesture</i>
          <input id="surname" type="text" name="surname" ng-model="$parent.account.surname" required aria-required="true"
                 minlength="2">
          <label for="surname">{{"loginRegisterPage.surname" | translate}}</label>
        </div>
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">lock_outline</i>
          <input id="password" type="password" name="password" ng-model="$parent.account.password" minlength="8">
          <label for="password">{{"loginRegisterPage.password" | translate}}</label>
        </div>
        <div class="col s12 m12 l6 input-field input-margin">
          <i class="material-icons prefix">lock_outline</i>
          <input id="passwordConfirm" type="password" name="passwordConfirm" ng-model="$parent.account.passwordConfirm"
                 minlength="8">
          <label for="passwordConfirm">{{"loginRegisterPage.confirmPassword" | translate}}</label>
          <div class="col s12 pull-l12" ng-if="edit"><i style="color: grey">{{"common.emptyPasswd" | translate}}</i></div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <div class="row">
        <div class="col s12 right-align">
          <div class="btn waves-effect waves-light red modal-close">{{"common.close" | translate}}
            <i class="material-icons right">close</i>
          </div>
          <button id="submitBtn" class="btn waves-effect waves-light" type="submit" name="action">{{"common.submit" | translate}}
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </div>
  </form>
</div>