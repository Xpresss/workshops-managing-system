<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Accounts</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:700" rel="stylesheet">
  <link rel="stylesheet" href="../../../css/style.css">
  <link rel="stylesheet" href="../../../css/spinner.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" ng-controller="accountsListController">

<div id="loader-wrapper">
  <div id="loader"></div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<input id="accountFromModel" ng-model="accountFromModel" value="${account}" type="hidden">

<header>
  <div ng-include="'/navigation'"></div>

  <nav class="white">
    <div class="nav-wrapper container truncate">
      <div class="row">
        <div class="col s12">
          <a href="/" class="breadcrumb teal-text"><i class="material-icons">home</i></a>
          <a href="/accounts" class="breadcrumb teal-text">{{"navigation.accounts" | translate}}</a>
          <a href="#!" class="breadcrumb teal-text">{{"common.list" |  translate}}</a>
        </div>
      </div>
    </div>
  </nav>
</header>

<main>
  <div class="container" style="margin-top: 35px">
    <div class="row">
      <div style="overflow-x:auto;">
        <table id="accountsTable" class="mdl-data-table highlight" cellspacing="0" style="width: 100%">
          <thead>
          <tr>
            <th>Login</th>
            <th>First name</th>
            <th>Surname</th>
            <th>Email</th>
            <th>Role</th>
            <th>Activated</th>
            <th>Enabled</th>
            <th></th>
          </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

  <div id="changeRoleModal" class="modal modal-fixed-footer" ng-init="getRoles()">
    <nav class="valign-wrapper">
      <h4 style="margin: auto">{{"accounts.changingRole" | translate}} <u>{{account.login}}</u></h4>
    </nav>
    <div class="modal-content valign-wrapper">
      <div class="row">
        <form id="changeRoleForm" class="col s12" name="changeRoleForm" novalidate="novalidate">
          <div class="row">
            <div class="s12">
              <label for="role">{{"accounts.role" | translate}}</label>
              <select id="role" class="icons">
                <option ng-repeat="role in roles" data-icon="./../../icons-images/{{role}}_icon.png" ng-value="role">{{"navigation." + role | translate}}</option>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer" ng-controller="editAccountController">
      <div class="row">
        <div class="col s12 right-align">
          <button class="btn waves-effect waves-light red modal-close" type="submit" name="action">{{"common.close" | translate}}
            <i class="material-icons right">close</i>
          </button>
          <button class="btn waves-effect waves-light" type="submit" name="action" ng-click="changeRole()">{{"common.submit" | translate}}
            <i class="material-icons right">send</i>
          </button>
        </div>
      </div>
    </div>
  </div>

  <div id="addAccount" ng-controller="addAccountController">
    <div ng-include="'accountModal'"></div>
  </div>

  <div id="editAccount" ng-controller="editAccountController">
    <div ng-include="'accountModal'"></div>
  </div>
</main>

<div ng-include="'/footer'"></div>

<script src="../../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.4.0/b-colvis-1.4.0/b-flash-1.4.0/b-html5-1.4.0/b-print-1.4.0/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.3.0/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="../../../js/EndpointService.js"></script>
<script src="../../../dependencies/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
<script src="../../../js/accountsControllers.js"></script>
<script src="../../../dependencies/slip.js"></script>
<script src="./../../../js/notificationsController.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
</body>
</html>
