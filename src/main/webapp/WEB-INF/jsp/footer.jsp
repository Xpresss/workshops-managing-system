<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="page-footer cyan">
  <div class="container">
    <div class="row">
      <div class="col s12">
        <span class="left">Praca inżynierska na kierunku Informatyka, wydział FTIMS Politechniki Łódzkiej.</span>
        <span class="right">Autor Dawid Dziedziczak 195576</span>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      <span class="right">© 2017 All rights reserved.</span>
    </div>
  </div>
</footer>
