<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Forgot password</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">
  <link rel="stylesheet" href="../../css/login-register-style.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" class="cyan" ng-controller="forgotPasswordController">
<div id="loader-wrapper">
  <div id="loader"></div>

  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>

</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<div class="row">
  <div class="col s 12 z-depth-4 card-panel">
    <form id="resetPasswordForm" class="login-form" novalidate>
      <div class="row">
        <div class="col s12 center input-field">
          <img src="./../../icons-images/training_icon4.png" class="yellow responsive-img logo">
          <p class="center login-form-title">{{ 'logo1' | translate }} <br> {{ 'logo2' | translate }}</p>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">person_outline</i>
          <input id="login" type="text" name="login" ng-model="login" required aria-required="true" >
          <label for="login">{{ 'loginRegisterPage.username' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">mail_outline</i>
          <input id="email" type="email" name="email" ng-model="email" required aria-required="true" >
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 center input-field">
          <button id="submitBtn" class="btn col s12 waves-effect waves-light">
            {{ 'loginRegisterPage.resetPasswordBtn' | translate }}
          </button>
        </div>
      </div>
      <div class="row">
        <div class="input-field">
          <p class="col s6 margin left-align medium-small">
            <a href="login">{{ 'loginRegisterPage.login' | translate }}</a>
          </p>
          <p class="col s6 margin right-align medium-small">
            <a href="register">{{ 'loginRegisterPage.register' | translate }}</a>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="./../../js/EndpointService.js"></script>
<script src="./../../js/forgotPasswordController.js"></script>
<script src="./../../dependencies/jquery.validate.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<script>
  jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.indexOf(" ") < 0;
  }, "Whitespaces are not allowed.");
</script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script>
  $.validator.setDefaults({
    errorClass: 'invalid',
    validClass: "valid",
    errorPlacement: function (error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']")
        .attr('data-error', error.text())
        .parent().addClass("error-margin");
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).removeClass(errorClass).addClass(validClass).parent().removeClass("error-margin");
      $(element.form).find("label[for=" + element.id + "]")
        .removeClass(errorClass);
    },
    submitHandler: function (form) {
      angular.element($('#submitBtn')).scope().submitBtnOnClick();
    }
  });

  $("#resetPasswordForm").validate({
    rules: {
      login: {
        noSpace: true
      },
      email: {
        noSpace: true
      },
    }
  });
</script>
</body>
</html>
