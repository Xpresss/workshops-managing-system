<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
  <title>Register</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
  <link rel="stylesheet" href="./../../css/style.css">
  <link rel="stylesheet" href="./../../css/spinner.css">
  <link rel="stylesheet" href="../../css/login-register-style.css">

  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body ng-app="app" class="cyan">
<div id="loader-wrapper">
  <div id="loader"></div>

  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>

</div>

<div id="spinner-wrapper">
  <div id="spinner"></div>
</div>

<div class="row" ng-controller="registerController">
  <div class="col s 12 z-depth-4 card-panel">
    <form id="registerForm" class="login-form" novalidate>
      <div class="row">
        <div class="col s12 center input-field">
          <img src="./../../icons-images/training_icon4.png" class="yellow responsive-img logo">
          <p class="center login-form-title">{{ 'logo1' | translate }} <br> {{ 'logo2' | translate }}</p>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">person_outline</i>
          <input id="login" type="text" name="login" ng-model="account.login" required aria-required="true" minlength="4">
          <label for="login">{{ 'loginRegisterPage.username' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">mail_outline</i>
          <input id="mail" type="email" name="email" ng-model="account.email" required aria-required="true">
          <label for="mail">Email</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">lock_outline</i>
          <input id="password" type="password" name="password" ng-model="account.password" required aria-required="true" minlength="8">
          <label for="password">{{ 'loginRegisterPage.password' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">lock_outline</i>
          <input id="passwordConfirm" type="password" name="passwordConfirm" ng-model="account.passwordConfirm" required aria-required="true" minlength="8">
          <label for="passwordConfirm">{{ 'loginRegisterPage.confirmPassword' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">border_color</i>
          <input id="firstName" type="text" name="firstName" ng-model="account.firstName" required aria-required="true" minlength="2">
          <label for="firstName">{{ 'loginRegisterPage.firstName' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 input-field">
          <i class="material-icons prefix">gesture</i>
          <input id="surname" type="text" name="surname" ng-model="account.surname" required aria-required="true" minlength="2">
          <label for="surname">{{ 'loginRegisterPage.surname' | translate }}</label>
        </div>
      </div>
      <div class="row margin">
        <div class="col s12 center input-field">
          <button id="registerBtn" class="btn col s12 waves-effect waves-light">
            {{ 'loginRegisterPage.registerNow' | translate }}
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col s12 input-field">
          <p class="margin center-align medium-small">
            {{ 'loginRegisterPage.haveAnAccount' | translate }}<a href="login">{{ 'loginRegisterPage.login' | translate }}</a>
          </p>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="./../../dependencies/jquery-3.2.1.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="./../../js/EndpointService.js"></script>
<script src="./../../js/registerController.js"></script>
<script src="./../../dependencies/jquery.validate.js"></script>
<script src="./../../dependencies/angular-translate.min.js"></script>
<script src="./../../js/translateProvider.js"></script>
<script>
  jQuery.validator.addMethod("noSpace", function(value, element) {
    return value.indexOf(" ") < 0;
  }, "Whitespaces are not allowed.");

  jQuery.extend(jQuery.validator.messages, {
    equalTo: "Passwords do not match.",
  });
</script>
<c:if test="${pageContext.response.locale == 'pl'}">
  <script src="./../../dependencies/jquery.validate.messages_pl.js"></script>
</c:if>
<script>
  $.validator.setDefaults({
    errorClass: 'invalid',
    validClass: "valid",
    errorPlacement: function (error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']")
        .attr('data-error', error.text())
        .parent().addClass("error-margin");
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).removeClass(errorClass).addClass(validClass).parent().removeClass("error-margin");
      $(element.form).find("label[for=" + element.id + "]")
        .removeClass(errorClass);
    },
    submitHandler: function (form) {
      console.log('form ok');
      angular.element($('#registerBtn')).scope().registerBtnOnClick();
    }
  });


  $("#registerForm").validate({
    rules: {
      passwordConfirm: {
        noSpace: true,
        equalTo: "#password"
      },
      password: {
        noSpace: true
      },
      login: {
        noSpace: true
      },
      email: {
        noSpace: true
      }
    }
  });
</script>
</body>
</html>