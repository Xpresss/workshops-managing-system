var app = angular.module('app', ['EndpointService', 'notificationsApp']);

document.addEventListener("DOMContentLoaded", function (event) {
  $('body').toggleClass('loaded');
});

app.controller('editAccountController', function ($scope, $rootScope, EndpointService, $translate) {

  $scope.$on('editAccount', function (event, data) {
    $scope.account = data;
    $("#password").val("");
    $("#passwordConfirm").val("");
    $(document).ready(function () {
      Materialize.updateTextFields();
    });
  });

  $scope.onFooterLoad = function () {
      EndpointService.getAccountToEdit($("#loginFromModel").val()).then(
        function (resp) {
          $scope.$emit("editAccount", resp.data);
        },
        function (resp) {
          if ($translate.use() === 'pl') {
            $translate(resp.data.error.code).then(function (t) {
              swal("", t, "error");
            });
          } else {
            swal("", resp.data.error.description, "error");
          }
        }
      )
  };

  $scope.submitBtnOnClick = function () {
    $('body').toggleClass('pending');
    EndpointService.editAccount($scope.account).then(
      function (resp) {
        $('body').toggleClass('pending');
        $translate("accountEdited").then(function (t) {
          swal(t, "", "success", {
            closeOnClickOutside: false,
            closeOnEsc: false
          }).then(() => {
            window.location.href = "/";
          });
        })
      },
      function (resp) {
        $('body').toggleClass('pending');
        if (resp.data.error == null) {
          $translate("Internal_server_error").then(function (t) {
            swal(t, "", "error");
          });
        } else {
          if ($translate.use() === 'pl') {
            $translate(resp.data.error.code).then(function (t) {
              swal("", t, "error");
            });
          } else {
            swal("", resp.data.error.description, "error");
          }
        }
      })
  };
});