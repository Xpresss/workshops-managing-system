var app = angular.module('app', ['EndpointService', 'UserIdentityService', 'at']);

document.addEventListener("DOMContentLoaded", function (event) {
  /*VANILLA JAVASCRIPT*/
  $('body').toggleClass('loaded');
});

app.controller('loginController', function ($scope, EndpointService, IdentitySharedProperty, $translate) {

  $(document).ready(function () {
    let activated = $("#activated").val();
    if (activated === 'true') {
      $translate('loginRegisterPage.accountActivated').then(function (t) {
        Materialize.toast(t, 10000);
      });
    }
  });

  $scope.performLogin = function () {
    $('body').toggleClass('pending');
    EndpointService.performLogin($scope.credentials).then(
      function (resp) {
        $('body').toggleClass('pending');
        console.log("*** SUCCESS ***");
        window.location.replace(resp.data);
      },
      function (resp) {
        $('body').toggleClass('pending');
        console.log("*** FAILURE ***");
          $translate(resp.data.message).then(function (t) {
            Materialize.toast(t, 8000);
          });
      });
  };

  $scope.$on('loginSuccess', function () {
    EndpointService.getIdentity().then(
      function (resp) {
        IdentitySharedProperty.setAccount(resp.data);
        console.log(IdentitySharedProperty.getAccount());
      },
      function (resp) {

      })
  });
});