var app = angular.module("UserIdentityService", []);

app.service('IdentitySharedProperty', function () {
  var account = {};
  return {
    getAccount: function () {
      return account;
    },
    setAccount: function (value) {
      account = value;
    }
  };
});