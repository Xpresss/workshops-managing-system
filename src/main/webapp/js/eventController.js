var app = angular.module('app', ['EndpointService', 'notificationsApp']);

document.addEventListener("DOMContentLoaded", function (event) {
  /*VANILLA JAVASCRIPT*/
  $('body').toggleClass('loaded');
});

app.controller("eventController", function ($scope, $rootScope, EndpointService, $translate) {
  $scope.addReview = function () {
    $('body').toggleClass('pending');
    EndpointService.addReview($("#eventId").val(), $scope.reviewToAdd).then(
      function () {
        $('body').toggleClass('pending');
        $translate("success").then(function (t1) {
          $translate("reviewAdded").then(function (t2) {
            swal(t1, t2, "success");
          });
        });
      },
      function (resp) {
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error");
          });
        } else {
          swal("", resp.data.error.description, "error");
        }
      }
    )
  };

  $scope.signUp = function (id) {
    $translate("signUpQuestion").then(function (t) {
      swal(t, {
        buttons: ["No", "Yes"],
      }).then((confirm) => {
          if (confirm) {
            $('body').toggleClass('pending');
            EndpointService.signUpForAnEvent(id, $("#loginFromModel").val()).then(
              function (resp) {
                $('body').toggleClass('pending');
                $translate("signedUp").then(function (t1) {
                  $translate("w84HR").then(function (t2) {
                    swal(t1, t2, "success")
                  });
                })
              },
              function (resp) {
                $('body').toggleClass('pending');
                if ($translate.use() === 'pl') {
                  $translate(resp.data.error.code).then(function (t) {
                    swal("", t, "error");
                  });
                } else {
                  swal("", resp.data.error.description, "error");
                }
              }
            )
            ;
          }
        }
      );
    });
  };

});

app.directive('eventType', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      const color = attr.eventType === 'TRAINING' ? 'red' : 'yellow';
      $(element)
        .attr('data-tooltip', attr.eventType.charAt(0).toUpperCase() + attr.eventType.slice(1).toLowerCase())
        .html(attr.eventType.charAt(0).toUpperCase())
        .addClass(color);
      $('.tooltipped').tooltip({delay: 150});
    }
  }
});