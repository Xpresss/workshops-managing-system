var app = angular.module('app', ['EndpointService', 'at']);

document.addEventListener("DOMContentLoaded", function (event) {
  /*VANILLA JAVASCRIPT*/
  $('body').toggleClass('loaded');
});

app.controller("forgotPasswordController", function ($scope, $rootScope, EndpointService, $translate) {
  $scope.submitBtnOnClick = function () {
    $('body').toggleClass('pending');
    EndpointService.restPassword($scope.login, $scope.email).then(
      function (resp) {
        $('body').toggleClass('pending');
        $translate("passwd_reset").then(function (t1) {
          $translate("passwd_reset2").then(function (t2) {
            swal(t1, t2, "success")
          })
        });
      },
      function (resp) {
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error");
          });
        } else {
          swal("", resp.data.error.description, "error");
        }
      }
    )
  }
});