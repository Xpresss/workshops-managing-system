$(document).ready(function () {
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    closeOnSelect: false, // Close upon selecting a date,
    min: new Date(),
    firstDay: 1,
    formatSubmit: 'yyyy-mm-dd',
    hiddenName: true
  });

  $('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    autoclose: false, // automatic close timepicker
    ampmclickable: false, // make AM PM clickable
    // aftershow: function () {
    // } //Function for after opening timepicker
  });
});