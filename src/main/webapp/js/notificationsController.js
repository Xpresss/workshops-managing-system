var app = angular.module('notificationsApp', ['EndpointService', 'at']);

app.controller("notificationsController", function ($scope, $rootScope, EndpointService) {
  $scope.dismissNotification = function (target) {
    EndpointService.dismissNotification($(target).attr("notification-id")).then(
      function (resp) {
        target.parentNode.removeChild(target);
        $("#notification-counter").html($("#notification-counter").html() - 1);
      },
      function (resp) {

      }
    )
  };

  $(document).ready(function () {
    function setupSlip(list) {
      list.addEventListener('slip:beforeswipe', function (e) {
        if (e.target.nodeName == 'INPUT' || e.target.classList.contains('no-swipe')) {
          e.preventDefault();
        }
      }, false);
      list.addEventListener('slip:beforewait', function (e) {
        if (e.target.classList.contains('instant')) e.preventDefault();
      }, false);
      list.addEventListener('slip:afterswipe', function (e) {
        $scope.dismissNotification(e.target);
      }, false);
      return new Slip(list);
    }

    setupSlip(document.getElementById('notifications-dropdown'));
  })
});