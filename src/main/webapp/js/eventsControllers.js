var app = angular.module('app', ['EndpointService', "notificationsApp"]);

document.addEventListener("DOMContentLoaded", function (event) {
  /*VANILLA JAVASCRIPT*/
  $('body').toggleClass('loaded');
});

var initTooltips = function () {
  $('.tooltipped').tooltip({delay: 150});
};

$(document).ready(function () {
  initTooltips();
});

app.controller('headEventController', function ($scope, $compile) {

  $(document).ready(function () {
    $('.modal').modal({});

    $('select').material_select();

    $('.tooltipped').tooltip({delay: 150});

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left', // Displays dropdown with edge aligned to the left of button
        stopPropagation: false // Stops event propagation
      }
    );

    /* ----- start of noUiSlider setup and initialization ----- */

    const slider = document.getElementById('capacity-slider');
    noUiSlider.create(slider, {
      start: 20,
      // connect: true,
      step: 1,
      // tooltips: [ false, wNumb({ decimals: 1 }), true ],
      orientation: 'horizontal',
      range: {
        'min': 0,
        'max': 100
      },
      format: wNumb({
        decimals: 0
      })
    });

    const capacityInput = document.getElementById('capacity-input');

    slider.noUiSlider.on('update', function (values, handle) {
      capacityInput.value = values[handle];
    });

    capacityInput.addEventListener('change', function () {
      slider.noUiSlider.set(this.value);
    });


    /* ----- end of noUiSlider ----- */

  });
});

app.controller('editEventController', function ($scope, $rootScope, EndpointService, $translate) {
  $scope.submitBtnOnClick = function () {
    $scope.event.date = $("input[name='date_input']").val();
    $scope.event.eventType = $('#event-type').val();
    $('body').toggleClass('pending');
    EndpointService.editEvent($scope.event).then(
      function (resp) {
        $('body').toggleClass('pending');
        $translate("success").then(function (t1) {
          $translate("eventEditedSuccessfully").then(function (t2) {
            swal(t1, t2, "success")
              .then((value) => {
                $("#editEventModal").modal('close');
              });
          })
        });
        $rootScope.$broadcast('refreshTable');
      }, function (resp) {
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error");
          });
        } else {
          swal("", resp.data.error.description, "error");
        }
      });
  };
});

app.controller('addEventController', function ($scope, $rootScope, EndpointService, $translate) {
  $scope.submitBtnOnClick = function () {
    $scope.event.date = $("input[name='date_input']").val();
    $scope.event.eventType = $('#event-type').val();
    $scope.event.capacity = $('#capacity-input').val();

    $('body').toggleClass('pending');

    EndpointService.addEvent($scope.event).then(
      function (resp) {
        $('body').toggleClass('pending');
        $translate("success").then(function (t1) {
          $translate("eventAddedSuccessfully").then(function (t2) {
            swal(t1, t2, "success", {
              closeOnClickOutside: false,
              closeOnEsc: false
            }).then(value => {
              delete $scope.event;
              const inputFields = $('#eventForm').find('input, select, textarea');
              inputFields.val("");
              inputFields.removeClass("invalid");
              inputFields.removeClass("valid");
              Materialize.updateTextFields();
            });
          });
        });
      }, function (resp) {
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error");
          });
        } else {
          swal("", resp.data.error.description, "error");
        }
      })
  };
});

app.controller('eventsGridController', function ($scope, $rootScope, EndpointService, $translate) {

  var URL = location.protocol;
  var PL = false;
  if ($translate.use() === 'pl') {
    PL = true;
    var langUrl = "//cdn.datatables.net/plug-ins/1.10.16/i18n/Polish.json";
  }

  var table;

  $translate("events.type").then(function (t1) {
    $translate("events.title").then(function (t2) {
      $translate("events.teacher").then(function (t3) {
        $translate("events.place").then(function (t4) {
          $translate("events.dateTime").then(function (t5) {
            $translate("events.description").then(function (t6) {
              $translate("events.personLimit").then(function (t7) {
                $translate("events.participants").then(function (t8) {

                  table = $('#eventsTable').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: true,
                    autoWidth: true,
                    "dom": '<"hide-on-med-and-up col s7"><"col s5 m3 offset-m9"f><rt><"col s3 m1"l><"col s2 hide-on-small-only"i><p>',
                    ajax: {
                      url: URL + '/events/getAllEventsToTable',
                      type: 'GET'
                    },
                    'columnDefs': [
                      {
                        'targets': -1,
                        'data': null,
                        'defaultContent': '' +
                        '<a id="detailBtn" class="btn-floating waves-effect waves-light white tooltipped" data-position="left" data-tooltip="Detail information"><i class="blue large material-icons">info_outline</i></a>' +
                        '<a id="editEventBtn" class="btn-floating waves-effect waves-light orange tooltipped" data-position="left" data-tooltip="Edit event" style="margin-left: 10px"><i class="large material-icons">mode_edit</i></a>' +
                        '<a id="removeEventBtn" class="btn-floating waves-effect waves-light red tooltipped" data-position="right" data-tooltip="Remove event" style="margin-left: 10px"><i class="large material-icons">delete</i></a>',

                        "className": "event-table-action-buttons-col-min-width"
                      },
                      {
                        'targets': [0, 3],
                        'visible': false
                      },
                      {
                        'targets': [1],
                        'data': null,
                        "render": function (data, type, full, meta) {
                          let eventType = "";
                          let color = "";
                          if (data[1] === 'TRAINING') {
                            if (PL) {
                              eventType = "S"
                            } else {
                              eventType = "T";
                            }
                            color = "red";
                          } else {
                            if (PL) {
                              eventType = "W"
                            } else {
                              eventType = "W";
                            }
                            color = "yellow";
                          }
                          return '<div class="btn-floating tooltipped ' + color + ' event-type-icon center-align standard-cursor" data-position="left"  data-tooltip="' + data[1].charAt(0).toUpperCase() + data[1].slice(1).toLowerCase() + '">' + eventType + '</div>';
                        },
                        "className": "event-type-col-min-width"
                      },
                      {'targets': 4, "className": "teacher-col-width"},
                      {'targets': 5, "className": "place-col-width"},
                      {'orderable': false, 'targets': 3},
                      {'orderable': false, 'targets': 6, "className": "date-time-col-width"},
                      {'orderable': false, 'targets': 7},
                      {'title': t1, 'targets': 1},
                      {'title': t2, 'targets': 2},
                      {'title': t3, 'targets': 4},
                      {'title': t4, 'targets': 5},
                      {'title': t5, 'targets': 6},
                    ],
                    "language": {
                      "url": langUrl
                    }
                  });

                  $scope.foo = function () {
                    console.log("foo");
                  };

                  $('#eventsTable tbody')
                    .on('click', '#editEventBtn', function () {
                      $('body').toggleClass('pending');
                      const data = table.row($(this).parents('tr')).data();
                      EndpointService.getEventToEdit(data[0]).then(function (resp) {
                        delete $rootScope.event;
                        const inputFields = $('#eventForm').find('input, select, textarea');
                        inputFields.removeClass("invalid");
                        inputFields.removeClass("valid");

                        $rootScope.event = resp.data;
                        $("#date").val($rootScope.event.date);
                        $("input[name='date_input']").val(moment($rootScope.event.date).format("YYYY-MM-DD"));
                        $('#event-type').val($rootScope.event.eventType);
                        $('#event-type').material_select();

                        Materialize.updateTextFields();
                        $("#editEventModal").modal("open");
                        $('body').toggleClass('pending');
                      }, function (resp) {
                        $('body').toggleClass('pending');
                        if ($translate.use() === 'pl') {
                          $translate(resp.data.error.code).then(function (t) {
                            swal("", t, "error");
                          });
                        } else {
                          swal("", resp.data.error.description, "error");
                        }
                      })
                    })
                    .on('click', '#removeEventBtn', function () {
                      const data = table.row($(this).parents('tr')).data();
                      EndpointService.getEventToEdit(data[0]).then(
                        function (resp) {
                          $rootScope.event = resp.data;
                          $translate("youSure").then(function (t1) {
                            $translate("llBeUnable").then(function (t2) {
                              swal({
                                title: t1,
                                text: t2,
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                                closeOnClickOutside: false,
                                closeOnEsc: false
                              })
                                .then((willDelete) => {
                                  if (willDelete) {
                                    $('body').toggleClass('pending');
                                    EndpointService.removeEvent(data[0]).then(
                                      function (resp) {
                                        $('body').toggleClass('pending');
                                        $scope.$emit("refreshTable");
                                        $translate("deleted").then(function (t1) {
                                          $translate("eventDeleted").then(function (t2) {
                                            swal(t1, t2, "success");
                                          });
                                        });
                                      },
                                      function (resp) {
                                        $('body').toggleClass('pending');
                                          if ($translate.use() === 'pl') {
                                          $translate(resp.data.error.code).then(function (t) {
                                            swal("", t, "error");
                                          });
                                        } else {
                                          swal("", resp.data.error.description, "error");
                                        }
                                      }
                                    );
                                  }
                                });
                            });
                          });
                        },
                        function (resp) {
                          if ($translate.use() === 'pl') {
                            $translate(resp.data.error.code).then(function (t) {
                              swal("", t, "error");
                            });
                          } else {
                            swal("", resp.data.error.description, "error");
                          }
                        }
                      );

                    })
                    .on('click', '#detailBtn', function () {
                      var tr = $(this).closest('tr');
                      var row = table.row(tr);

                      if (row.child.isShown()) {
                        // This row is already open - close it
                        $(this).attr("data-tooltip", "Detail information").tooltip({delay: 150}).find("i").removeClass("white").addClass("blue").css("color", "white");
                        row.child.hide();
                        tr.removeClass('shown');
                      }
                      else {
                        // Open this row
                        $(this).attr("data-tooltip", "Close details").tooltip({delay: 150}).find("i").removeClass("blue").addClass("white").css("color", "blue");
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                      }
                    })
                    .on("click", "#participantsBtn", function () {
                      const data = table.row($(this).closest('tr').prev()).data();
                      $rootScope.eventId = data[0];
                      EndpointService.getEventsParicipants(data[0]).then(
                        function (resp) {
                          $rootScope.participants = resp.data;
                          $rootScope.eventTitleForParticipantsModal = data[2];
                          $("#participants").modal("open");
                        },
                        function (resp) {
                          if ($translate.use() === 'pl') {
                            $translate(resp.data.error.code).then(function (t) {
                              swal("", t, "error");
                            });
                          } else {
                            swal("", resp.data.error.description, "error");
                          }
                        });
                    });

                  function format(d) {
                    // `d` is the original data object for the row
                    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                      '<tr>' +
                      '<td style="width: 100px;"><b>' + t6 + ':</b></td>' +
                      '<td>' + d[3] + '</td>' +
                      '</tr>' +
                      '<tr>' +
                      '<td><b>' + t7 + '</b></td>' +
                      '<td>' + d[7] + '</td>' +
                      '</tr>' +
                      '</table>' +
                      '<div id="participantsBtn" class="btn teal accent-4 waves-effect waves-light"><i class="right material-icons">people</i>' + t8 + '</div>';
                  }

                  $scope.getEventsByType = function (event, type) {
                    switch (type) {
                      case 'ALL':
                        table.ajax.url(URL + '/events/getAllEventsToTable').load();
                        break;
                      case 'WORKSHOPS':
                        table.ajax.url(URL + '/events/getOnlyWorkshopsToTable').load();
                        break;
                      case 'TRAININGS':
                        table.ajax.url(URL + '/events/getOnlyTrainingsToTable').load();
                        break;
                    }

                    $('div[name=eventTypeBtn]').each(function () {
                      $(this).removeClass('disabled');
                    });
                    $(event.target).addClass('disabled');
                  };
                });
              });
            });
          });
        });
      });
    });
  });

  $scope.removeParticipant = function (login) {
    $translate("youSure").then(function (t1) {
      $translate("llBeUnable").then(function (t2) {
        swal({
          title: t1,
          text: t2,
          icon: "warning",
          buttons: true,
          dangerMode: true,
          closeOnClickOutside: false,
          closeOnEsc: false
        })
          .then((willDelete) => {
            if (willDelete) {
              $('body').toggleClass('pending');
              EndpointService.removeParticipant(login, $rootScope.eventId).then(
                function (resp) {
                  $('body').toggleClass('pending');
                  $translate("deleted").then(function (t1) {
                    $translate("participantDeleted").then(function (t2) {
                      swal(t1, t2, "success").then(() => {
                        $("#participants").modal("close");
                      })
                    });
                  });
                },
                function (resp) {
                  $('body').toggleClass('pending');
                  if (resp.data.error == null) {
                    $translate("Internal_server_error").then(function (t1) {
                      swal(t1, "", "error");
                    });
                  } else {
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error");
                      });
                    } else {
                      swal("", resp.data.error.description, "error");
                    }
                  }
                }
              );
            }
          });
      })
    });
  };

  $scope.openTab = function () {
    table.ajax.url(URL + '/events/getAllEventsToTable').load();
  };

  $scope.refreshTable = function () {
    table.ajax.reload(null, false);
  };

  $scope.$on('refreshTable', function () {
    $scope.refreshTable();
  });
});


app.controller('signUpRequestsController', function ($scope, $rootScope, EndpointService, $translate) {

  $scope.init = function () {
    $scope.$emit("getSignUps");
  };

  $scope.$on("getSignUps", function () {
    EndpointService.getAllSignUpRequests().then(
      function (resp) {
        $scope.requests = resp.data;
      },
      function (resp) {
        window.location.href = "/500";
      });

  });

  $scope.considerSignUpRequest = function (eventId, login, isConfirmed) {
    EndpointService.getSignUpRequestToEdit(eventId, login).then(
      function (resp) {
        if (resp.data) {
          $translate("alreadyAccepted").then(function (t) {
            swal(t, "", "warning")
          });
        } else {
          $translate("consider" + isConfirmed).then(function (t) {
            swal(t, {
              buttons: ["No", "Yes"],
            }).then((confirm) => {
              if (confirm) {
                $('body').toggleClass('pending');
                EndpointService.considerSignUpRequest(eventId, login, isConfirmed).then(
                  function (resp) {
                    $('body').toggleClass('pending');
                    $scope.$emit("getSignUps");
                    $translate("success").then(function (t) {
                      swal(t, resp.data, "success");
                    });
                  },
                  function (resp) {
                    $('body').toggleClass('pending');
                    $scope.$emit("getSignUps");
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error");
                      });
                    } else {
                      swal("", resp.data.error.description, "error");
                    }
                  }
                )
              }
            });
          });
        }
      }, function () {
      });
  };
});

app.controller('participantsController', function ($scope, $rootScope, EndpointService, $translate) {
  $scope.removeParticipant = function (login) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to undo this action!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      closeOnClickOutside: false,
      closeOnEsc: false
    })
      .then((willDelete) => {
        if (willDelete) {
          $('body').toggleClass('pending');
          EndpointService.removeParticipant(login).then(
            function (resp) {
              $('body').toggleClass('pending');
              $translate("deleted").then(function (t1) {
                $translate("participantDeleted").then(function (t2) {
                  swal(t1, t2, "success");
                });
              });
            },
            function (resp) {
              $('body').toggleClass('pending');
              if ($translate.use() === 'pl') {
                $translate(resp.data.error.code).then(function (t) {
                  swal("", t, "error");
                });
              } else {
                swal("", resp.data.error.description, "error");
              }
            }
          );
        }
      });
  }
});