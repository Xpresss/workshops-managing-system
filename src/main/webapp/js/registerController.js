var app = angular.module('app', ['EndpointService', 'at']);

document.addEventListener("DOMContentLoaded", function (event) {
  /*VANILLA JAVASCRIPT*/
  $('body').toggleClass('loaded');
});

app.controller('registerController', function ($scope, EndpointService, $translate) {

  $scope.registerBtnOnClick = function () {
    $('body').toggleClass('pending');
    delete $scope.account.passwordConfirm;
    EndpointService.register($scope.account).then(
      function (resp) {
        $('body').toggleClass('pending');
        $translate("Account_registered").then(function (t) {
          swal(t, "", "success");
        })
      },
      function (resp) {
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error");
          });
        } else {
          swal("", resp.data.error.description, "error");
        }
      })
  }
});