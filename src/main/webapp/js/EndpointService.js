var module = angular.module('EndpointService', []);

module.factory('EndpointService', function ($http) {
  var URL = location.protocol;
  var factory = {
    getAllEvents: function () {
      return $http.get(URL + '/events/get');
    },
    getEventToEdit: function (id) {
      return $http.get(URL + "/events/getToEdit/" + id);
    },
    editEvent: function (event) {
      return $http.put(URL + "/events/edit/" + event.id, event);
    },
    removeEvent: function (id) {
      return $http.delete(URL + "/events/remove/" + id);
    },
    addEvent: function (event) {
      return $http.post(URL + "/events/create", event);
    },
    register: function (account) {
      return $http.post(URL + '/account/register', account)
    },
    getAccountToEdit: function (login) {
      return $http.get(URL + '/account/getAccountToEdit', {params: {login: login}});
    },
    editAccount: function (account) {
      return $http.put(URL + '/account/edit', account);
    },
    getRoles: function () {
      return $http.get(URL + '/account/getRoles');
    },
    addAccount: function (account) {
      return $http.post(URL + '/account/create', account, {params: {login: account.login}})
    },
    enableAccount: function (login) {
      return $http.put(URL + '/account/enable', null, {params: {login: login}});
    },
    disableAccount: function (login) {
      return $http.put(URL + '/account/disable', null, {params: {login: login}});
    },
    performLogin: function (credentials) {
      var config = {
        params: {
          username: credentials.username,
          password: credentials.password
        },
        ignoreAuthModule: 'ignoreAuthModule'
      };
      return $http.post(URL + 'perform-login', '', config);
    },
    getIdentity: function () {
      return $http.get(URL + 'security/account');
    },
    signUpForAnEvent: function (id, login) {
      return $http.post(URL + '/events/sign-up', null, {params: {eventId: id, login: login}});
    },
    getAllSignUpRequests: function () {
      return $http.get(URL + '/events/sign-up-requests/get');
    },
    getSignUpRequestToEdit: function (eventId, login) {
      return $http.get(URL + "/events/sign-up-requests/to-edit", {params: {eventId: eventId, login: login}})
    },
    considerSignUpRequest: function (eventId, login, isConfirmed) {
      return $http.put(URL + '/events/sign-up-requests/consider', null, {
        params: {
          eventId: eventId,
          login: login,
          isConfirmed: isConfirmed
        }
      });
    },
    getUsersEvents: function (login) {
      return $http.get(URL + "/events/userEvents", {params: {login: login}});
    },
    getOncomingEvents: function () {
      return $http.get(URL + "/events/oncomingEvents");
    },
    getEventsToRate: function (login) {
      return $http.get(URL + "/events/toRate", {params: {login: login}});
    },
    getEventsParicipants: function (eventId) {
      return $http.get(URL + "/events/participants", {params: {eventId: eventId}});
    },
    searchEvents: function (filter) {
      return $http.get(URL + "/events/search", {params: {filter: filter}});
    },
    removeParticipant(login, eventId) {
      return $http.delete(URL + "/events/removeParticipant", {params: {login: login, eventId: eventId}});
    },
    addReview: function (eventId, review) {
      return $http.post(URL + "/events/addReview", review, {params: {eventId: eventId}});
    },
    restPassword: function (login, email) {
      return $http.put(URL + "/account/reset-password", "", {params: {login: login, email: email}});
    },
    dismissNotification: function (notificationId) {
      return $http.put(URL + "/account/dismiss-notification", "", {params: {notificationId: notificationId}});
    }
  };
  return factory
});
