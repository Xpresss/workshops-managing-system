var app = angular.module('app', ['EndpointService', "notificationsApp"]);

$(document).ready(function () {
  $('.tooltipped').tooltip({delay: 150});

  $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );

  $("#filter-tabs").hide();
  $("#search-input-field").hide();

  $("#filter-btn").on("click", function () {
    leaving = false;
    $(this).fadeOut(250, function () {
      $("#filter-tabs").show("slide", {direction: "right"}, 500, function () {
        $(this).focus();
      });
    });
    $("#search-btn").hide();
    $("#search-input-field").hide();
  });

  let leaving = false;

  $("#filter-tabs").mouseleave(function () {
    if (!leaving) {
      leaving = true;
      $(this).fadeOut(500, function () {
        $("#filter-btn").animate({width: 'toggle'}, 500, function () {
          $("#search-btn").animate({width: 'toggle'}, 500);
        });
      });
    }
  });

  $("#search-btn").on("click", function () {
    clearSearchInput();
    $(this).fadeOut(250, function () {
      $("#search-input-field").show("slide", {direction: "right"}, 500, function () {
        $(this).find("input").focus();
      });
    });
    $("#filter-btn").animate({width: 'toggle'}, 500);
  });

  $("#search-input-field").focusout(function () {
    $(this).fadeOut(500, function () {
      $("#search-btn").animate({width: 'toggle'}, 500);
      $("#filter-btn").animate({width: 'toggle'}, 500);
    });
  });

  function clearSearchInput() {
    $("#search-input-field").find("input").val("").parent().find("label").removeClass("active");
  }
});

app.controller('eventsController', function ($scope, $rootScope, EndpointService, $translate) {
  let eventsInCards;

  $scope.init = function () {
    $scope.canSignUp = true;

    EndpointService.getAllEvents().then(
      function (resp) {
        $rootScope.events = [];
        jQuery.each(resp.data, function (index, value) {
          value.canSignUp = true;
          $rootScope.events.push(value);
        });
        $scope.noResults = $rootScope.events.length === 0;
        setTimeout(function () {
          resizer()
        }, 500);
        $('body').toggleClass('loaded');
      },
      function (resp) {
        window.location.href = "/500";
      }
    );
    eventsInCards = $("#events-in-cards");
  };

  $scope.signUp = function (id) {
    $translate("signUpQuestion").then(function (t) {
      swal(t, {
        buttons: ["No", "Yes"],
      }).then((confirm) => {
        if (confirm) {
          $('body').toggleClass('pending');
          EndpointService.signUpForAnEvent(id, $("#loginFromModel").val()).then(
            function (resp) {
              $('body').toggleClass('pending');
              $translate("signedUp").then(function (t1) {
                $translate("w84HR").then(function (t2) {
                  swal(t1, t2, "success")
                });
              });
            },
            function (resp) {
              $('body').toggleClass('pending');
              if ($translate.use() === 'pl') {
                $translate(resp.data.error.code).then(function (t) {
                  swal("", t, "error");
                });
              } else {
                swal("", resp.data.error.description, "error");
              }
            });
        }
      });
    });
  };

  $("#searchForm").submit(function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    $scope.searchEvents();
  });

  $scope.searchEvents = function () {
    eventsInCards.fadeOut();
    $('body').toggleClass('pending');
    EndpointService.searchEvents($scope.filter).then(
      function (resp) {
        onFilterSuccess(resp, true);
      },
      function (resp) {
        onFilterFailure(resp);
      }
    );
  };

  $scope.showAllEvents = function () {
    eventsInCards.fadeOut();
    $('body').toggleClass('pending');
    EndpointService.getAllEvents().then(
      function (resp) {
        onFilterSuccess(resp, true);
      }, function (resp) {
        onFilterFailure(resp);
      }
    );
  };

  $scope.showOncomingEvents = function () {
    eventsInCards.fadeOut();
    $('body').toggleClass('pending');
    EndpointService.getOncomingEvents().then(
      function (resp) {
        onFilterSuccess(resp, true);
      }, function (resp) {
        onFilterFailure(resp);
      }
    );
  };

  $scope.showMyEvents = function () {
    eventsInCards.fadeOut();
    $('body').toggleClass('pending');
    EndpointService.getUsersEvents($("#loginFromModel").val()).then(
      function (resp) {
        onFilterSuccess(resp, false);
      },
      function (resp) {
        onFilterFailure(resp);
      }
    );
  };

  $scope.showEventsToRate = function () {
    eventsInCards.fadeOut();
    $('body').toggleClass('pending');
    EndpointService.getEventsToRate($("#loginFromModel").val()).then(
      function (resp) {
        onFilterSuccess(resp, false);
      }, function (resp) {
        onFilterFailure(resp);
      }
    );
  };

  $scope.goToReviews = function (id) {
    window.location.href = "/event?id=" + id;
  };

  let onFilterSuccess = function (resp, canSignUp) {
    $rootScope.events = [];
    jQuery.each(resp.data, function (index, value) {
      value.canSignUp = canSignUp;
      $rootScope.events.push(value);
    });
    $scope.noResults = $rootScope.events.length === 0;
    $('body').toggleClass('pending');
    eventsInCards.show("clip", 500, resizer());
  };

  let onFilterFailure = function (resp) {
    $('body').toggleClass('pending');
    window.location.href = "/500";
  };
});

app.directive('eventType', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      const color = attr.eventType === 'TRAINING' ? 'red' : 'yellow';
      $(element)
        .attr('data-tooltip', attr.eventType.charAt(0).toUpperCase() + attr.eventType.slice(1).toLowerCase())
        .html(attr.eventType.charAt(0).toUpperCase())
        .addClass(color);
      $('.tooltipped').tooltip({delay: 150});
    }
  }
});

function resizer() {
  $(document).ready(function () {
    $(".to-resize").each(function () {
      // resize_to_fit($(this));
      resize_to_fit_iter($(this));
    })
  });
}

function resize_to_fit(span) {
  let fontsize = span.css('font-size');
  span.css('fontSize', parseFloat(fontsize) - 1);

  let height = span.parent().outerHeight();
  if (height > 88 && height < 124 || height > 124) {
    resize_to_fit(span);
  }
}

function resize_to_fit_iter(span) {
  let fontsize = parseFloat(span.css('font-size'));
  let height = span.parent().outerHeight();

  while (height > 88 && height < 124 || height > 124) {
    fontsize--;
    span.css('fontSize', fontsize);
    height = span.parent().outerHeight();
  }
}

$(window).resize(function () {
  resizer();
});