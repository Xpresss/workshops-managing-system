var app = angular.module('app', ['EndpointService', 'notificationsApp', 'at']);

document.addEventListener("DOMContentLoaded", function (event) {
  $('body').toggleClass('loaded');
});

$(document).ready(function () {
  $('.tooltipped').tooltip({delay: 150});
  $('.modal').modal({});
  $('select').material_select();

  $('.dropdown-button').dropdown({
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    }
  );


  initValidation();

  $('#role').val("");
});

let trueOrFalse = function (data, type, row, meta) {
  if (row[6] !== 'true') {
    $('tbody tr:nth-child(' + (meta.row + 1) + ')').addClass('banned');
  }

  const icon = data === 'true' ? "check" : "close";
  return '<i class="material-icons">' + icon + '</i>'
};

let isBanned = function (data, type, row, meta) {
  $(document).ready(function () {
    if (data !== 'true') {
      $('tbody tr:nth-child(' + (meta.row + 1) + ')').addClass('banned');
    }
  });
  return null;
};

let renderRoleIcons = function (data, type, row) {
  return '<img src="./../../icons-images/' + data + '_icon.png" alt="' + data + '" height="50" width="50">';
};

let renderActionButtons = function (data, type, row) {

  let buttons = '<a id="editAccountBtn" class="btn-floating waves-effect waves-light orange tooltipped" data-position="left" data-tooltip="Edit account"><i class="large material-icons">edit</i></a>' +
    '<a id="changeRoleBtn" class="btn-floating waves-effect waves-light indigo accent-2 tooltipped" style="margin-left: 10px" data-position="left" data-tooltip="Change role"><i class="large material-icons">supervisor_account</i></a>';
  if (row[6] === 'true') {
    buttons += '<a id="disableBtn" class="btn-floating waves-effect waves-light red tooltipped" style="margin-left: 10px" data-position="right" data-tooltip="Disable account"><i class="large material-icons">not_interested</i></a>';
  } else {
    buttons += '<a id="enableBtn" class="btn-floating waves-effect waves-light green tooltipped" style="margin-left: 10px" data-position="right" data-tooltip="Enable account"><i class="large material-icons">lock_open</i></a>';
  }
  return buttons;
};

app.controller('accountsListController', function ($scope, $rootScope, EndpointService, $translate) {

  $(document).ready(function () {
    $translate("accounts.new").then(function (t) {
      $("div.toolbar").html('<a class="btn waves-effect waves-light green accent-4" id="addAccountBtn" type="button" name="addAccountBtn">' + t + '<i class="material-icons left">add</i></a>');

      $('#addAccountBtn').on('click', function () {
        $('#addAccount').find('#accountModal').modal({});
        initModals();
        $(document).ready(function () {
          $scope.account = null;
          $('#addAccount').find('#accountModal').modal('open');
        });
      });
    });
  });

  var URL = location.protocol;
  var PL = false;
  if ($translate.use() === 'pl') {
    PL = true;
    var langUrl = "//cdn.datatables.net/plug-ins/1.10.16/i18n/Polish.json";
  }

  var table;

  $translate("accounts.login").then(function (t1) {
    $translate("loginRegisterPage.firstName").then(function (t2) {
      $translate("loginRegisterPage.surname").then(function (t3) {
        $translate("accounts.role").then(function (t4) {
          $translate("accounts.activated").then(function (t5) {

            table = $('#accountsTable').DataTable({
              processing: true,
              serverSide: true,
              searching: true,
              autoWidth: true,
              "dom": '<"col s4 toolbar"><"col s6 m4 l3 offset-s2 offset-m4 offset-l5"f><rt><"col s3 m1"l><"col s2 hide-on-small-only"i><p>',
              ajax: {
                url: URL + '/account/getAccountsToTable',
                type: 'GET'
              },
              'columnDefs': [
                {
                  'targets': -1,
                  'sortable': false,
                  'data': null,
                  'className': 'buttons-column',
                  'render': renderActionButtons,
                },
                {
                  "targets": 4, "render": renderRoleIcons, "className": "center-align role-column-min-width"
                },
                {
                  "targets": 5, "render": trueOrFalse, "className": "center-align", 'sortable': false
                },
                {
                  "targets": 6, "render": isBanned, visible: false
                },
                {
                  "targets": [0, 1, 2, 3], "className": "column-min-width"
                },
                {'title': t1, 'targets': 0},
                {'title': t2, 'targets': 1},
                {'title': t3, 'targets': 2},
                {'title': t4, 'targets': 4},
                {'title': t5, 'targets': 5},
              ],
              "language": {
                "url": langUrl
              }
            });

            $('#accountsTable tbody')
              .on('click', '#changeRoleBtn', function () {
                $('body').toggleClass('pending');
                const data = table.row($(this).parents('tr')).data();
                EndpointService.getAccountToEdit(data[0]).then(
                  function (resp) {
                    $rootScope.account = resp.data;
                    $('#role').val($rootScope.account.role);
                    $("#changeRoleModal").modal("open");
                    $('#role').material_select();
                    $('body').toggleClass('pending');
                  },
                  function (resp) {
                    $('body').toggleClass('pending');
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error", {
                          closeOnClickOutside: false,
                          closeOnEsc: false
                        })
                      })
                    } else {
                      swal("", resp.data.error.description, "error", {
                        closeOnClickOutside: false,
                        closeOnEsc: false
                      });
                    }
                  });
              })
              .on('click', '#editAccountBtn', function () {
                $('body').toggleClass('pending');
                const data = table.row($(this).parents('tr')).data();
                EndpointService.getAccountToEdit(data[0]).then(
                  function (resp) {
                    initModals();

                    $scope.$broadcast('editAccount', resp.data);
                    $rootScope.login = resp.data.login;

                    $('#editAccount').find('#accountModal').modal();
                    $(document).ready(function () {
                      $('#editAccount').find('#accountModal').modal('open');
                    });
                    $('body').toggleClass('pending');
                  },
                  function (resp) {
                    $('body').toggleClass('pending');
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error", {
                          closeOnClickOutside: false,
                          closeOnEsc: false
                        })
                      })
                    } else {
                      swal("", resp.data.error.description, "error", {
                        closeOnClickOutside: false,
                        closeOnEsc: false
                      });
                    }
                  });
              })
              .on('click', '#enableBtn', function () {
                $('body').toggleClass('pending');
                const data = table.row($(this).parents('tr')).data();
                EndpointService.enableAccount(data[0]).then(
                  function (resp) {
                    $('body').toggleClass('pending');
                    $scope.refreshTable();
                    $translate("accountEnabled").then(function (t) {
                      swal(t, "", "success", {closeOnClickOutside: false, closeOnEsc: false});
                    });
                  },
                  function (resp) {
                    $('body').toggleClass('pending');
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error", {
                          closeOnClickOutside: false,
                          closeOnEsc: false
                        })
                      })
                    } else {
                      swal("", resp.data.error.description, "error", {
                        closeOnClickOutside: false,
                        closeOnEsc: false
                      });
                    }
                  })
              })
              .on('click', '#disableBtn', function () {
                $('body').toggleClass('pending');
                const data = table.row($(this).parents('tr')).data();
                EndpointService.disableAccount(data[0]).then(
                  function (resp) {
                    $('body').toggleClass('pending');
                    $scope.refreshTable();
                    $translate("accountDisabled").then(function (t) {
                      swal(t, "", "success", {closeOnClickOutside: false, closeOnEsc: false});
                    });
                  },
                  function (resp) {
                    $('body').toggleClass('pending');
                    if ($translate.use() === 'pl') {
                      $translate(resp.data.error.code).then(function (t) {
                        swal("", t, "error", {
                          closeOnClickOutside: false,
                          closeOnEsc: false
                        })
                      })
                    } else {
                      swal("", resp.data.error.description, "error", {
                        closeOnClickOutside: false,
                        closeOnEsc: false
                      });
                    }
                  })
              });

            $("#accountsTable").on('page.dt', function () {
              $(document).ready(function () {
                $('.tooltipped').tooltip({delay: 150});
              });
            });
          });
        });
      });
    });
  });

  $scope.getRoles = function () {
    EndpointService.getRoles().then(
      function (resp) {
        $scope.roles = resp.data;
        $('#role').material_select();
      },
      function (resp) {
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error", {
              closeOnClickOutside: false,
              closeOnEsc: false
            })
          })
        } else {
          swal("", resp.data.error.description, "error", {
            closeOnClickOutside: false,
            closeOnEsc: false
          });
        }
      }
    );
  };

  $scope.refreshTable = function () {
    table.ajax.reload(null, false);
  };

  $scope.$on('refreshTable', function () {
    $scope.refreshTable();
  });
});

app.controller('editAccountController', function ($scope, $rootScope, EndpointService, $translate) {
  $scope.edit = true;

  $translate("accounts.modalHeader.edit").then(function (t) {
    $scope.modalHeader = t;
  });

  $scope.$on('editAccount', function (event, data) {
    $scope.account = data;
    $("#password").val("");
    $("#passwordConfirm").val("");
    $(document).ready(function () {
      Materialize.updateTextFields();
    });
  });

  $(document).ready(function () {
    const editAccount = $("#editAccount");
    editAccount.find('#accountModal').modal({
      complete: function () {
        delete $scope.account;
        const inputFields = editAccount.find('#accountForm').find('input, select, textarea');
        inputFields.removeClass("invalid");
        inputFields.removeClass("valid");
        Materialize.updateTextFields();
      }
    });
  });

  $scope.changeRole = function () {
    $('body').toggleClass('pending');
    let role;
    if ((role = $('#role').val()) !== "" && role !== undefined) {
      $scope.account.role = $('#role').val();
    }
    EndpointService.editAccount($scope.account).then(
      function (resp) {
        $scope.refreshTable();
        $('body').toggleClass('pending');
        $translate("roleChanged").then(function (t) {
          swal(t, "", "success", {
            closeOnClickOutside: false,
            closeOnEsc: false
          }).then(() => {
            $('#changeRoleModal').modal('close');
          });
        });
      },
      function (resp) {
        $scope.refreshTable();
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error", {
              closeOnClickOutside: false,
              closeOnEsc: false
            })
          })
        } else {
          swal("", resp.data.error.description, "error", {
            closeOnClickOutside: false,
            closeOnEsc: false
          });
        }
      }
    )
  };

  $scope.submitBtnOnClick = function () {
    $('body').toggleClass('pending');
    EndpointService.editAccount($scope.account).then(
      function (resp) {
        $('body').toggleClass('pending');
        $scope.refreshTable();
        $translate("accountEdited").then(function (t) {
          swal(t, "", "success", {
            closeOnClickOutside: false,
            closeOnEsc: false
          }).then(() => {
            $("#editAccount #accountModal").modal('close');
          });
        });
      },
      function (resp) {
        $scope.refreshTable();
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error", {
              closeOnClickOutside: false,
              closeOnEsc: false
            })
          })
        } else {
          swal("", resp.data.error.description, "error", {
            closeOnClickOutside: false,
            closeOnEsc: false
          });
        }
      })
  };
});

app.controller('addAccountController', function ($scope, $rootScope, EndpointService, $translate) {
  $scope.edit = false;
  $translate("accounts.modalHeader.new").then(function (t) {
    $scope.modalHeader = t;
  });
  $(document).ready(function () {
    const addAccount = $("#addAccount");
    addAccount.find('#accountModal').modal({
      complete: function () {
        delete $scope.account;
        const inputFields = addAccount.find('#accountForm').find('input, select, textarea');
        inputFields.val('');
        inputFields.removeClass("invalid");
        inputFields.removeClass("valid");
        Materialize.updateTextFields();
      }
    });
  });

  $scope.submitBtnOnClick = function () {
    $('body').toggleClass('pending');
    EndpointService.addAccount($scope.account).then(
      function (resp) {
        $scope.refreshTable();
        $('body').toggleClass('pending');
        $translate("accountAdded").then(function (t) {
          swal(t, "", "success", {
            closeOnClickOutside: false,
            closeOnEsc: false
          }).then((value) => {
            $("#addAccount #accountModal").modal('close');
          });
        });
      },
      function (resp) {
        $scope.refreshTable();
        $('body').toggleClass('pending');
        if ($translate.use() === 'pl') {
          $translate(resp.data.error.code).then(function (t) {
            swal("", t, "error", {
              closeOnClickOutside: false,
              closeOnEsc: false
            })
          })
        } else {
          swal("", resp.data.error.description, "error", {
            closeOnClickOutside: false,
            closeOnEsc: false
          });
        }
      })
  };
});

/* ************* */

function initValidation() {
  $.validator.setDefaults({
    errorClass: 'invalid',
    validClass: "valid",
    errorPlacement: function (error, element) {
      $(element)
        .closest("form")
        .find("label[for='" + element.attr("id") + "']")
        .attr('data-error', error.text());
    }
  });
  jQuery.extend(jQuery.validator.messages, {
    equalTo: "Passwords do not match.",
  });


  jQuery.validator.addMethod("noSpace", function (value, element) {
    return value.indexOf(" ") < 0;
  }, "Whitespaces are not allowed.");
}

function initModals() {

  const editAccount = $("#editAccount");
  var validator = editAccount.find("#accountForm").validate({
    rules: {
      passwordConfirm: {
        noSpace: true,
        equalTo: "#editAccount #password"
      },
      password: {
        noSpace: true
      },
      login: {
        noSpace: true
      },
      email: {
        noSpace: true
      }
    },
    submitHandler: function () {
      angular.element(editAccount.find('#submitBtn')).scope().submitBtnOnClick();
    }
  });

  const addAccount = $("#addAccount");
  addAccount.find("#accountForm").validate({
    rules: {
      password: {
        required: true,
        noSpace: true
      },
      passwordConfirm: {
        required: true,
        equalTo: "#addAccount #password",
        noSpace: true
      },
      login: {
        noSpace: true
      },
      email: {
        noSpace: true
      }
    },
    submitHandler: function () {
      angular.element(addAccount.find('#submitBtn')).scope().submitBtnOnClick();
    }
  });
}